<?php
/**
 * @author zzq
 * @brief 得到文章所有分类
 */
function get_all_service_type_helper($params) {
	$types = ServiceModel::getAllTypeById($params['show'], $params['parent_id']);
	$id = $params['parent_id'];
    $type1 = array();
    $content_1 = "var type_1=new Array(";
    $content_2 = "var type_2=new Array(";
    
    foreach($types as $key => $value) {
        if($value['parent_id'] == $id) {
            $type1[]   = $value['id'];
            $content_1.= "new Array(".$value['id'].",'".$value['name']."'),";
        } else if(in_array($value['parent_id'], $type1)) {
            $content_2.= "new Array(".$value['parent_id'].",".$value['id'].",'".$value['name']."'),";
        }
    }
    $content = rtrim($content_1, ",").");".rtrim($content_2, ",").");";
    $con = '<p>分类：
               <span id="sort_msg1"></span>
               <span id="sort_msg2"></span>
           </p>
           <select size=7 name="type_1" id="sel1">
               <option  value="0">请选择分类</option>';
foreach($types as $key => $value) {
    if ($value['parent_id'] != $id ) {
        continue;
    }
    $con.='<option value="'.$value['id'].'"'; 
    if ($value["id"] == $params["type"]) { 
        $con.=' selected '; 
    }
     $con.= '>'.$value["name"].'</option>';
}
$con.='</select>
<select size=7 name="type_2" id="sel2" style="display: none; ">
</select>';
$con .=  '<script>'.$content.'</script>
 <script>
$(function(){
    var edit   = 0;
    
    var type2 = '.intval($params['stype']).';

    $(document).ready(function(){
        
        $("#sel1").change(function(){
            var sel1_id=$(this).val();
            $("#sort_msg2").html("");
            if(sel1_id==0){
                $("#sel2").hide();
                $("#sort_msg1").html("");
                
                return false;
            }
            
            $("#sel2").show();
            $("#sel2").html("<option value=0>请选择分类</option>");
            for(var i=0;i<type_2.length;i++){
                if(type_2[i][0] == sel1_id){
                    if (edit ==1 ) {
                        if (type2 == type_2[i][1]) {
                         
                            $("#sel2").append("<option value="+type_2[i][1]+" selected>"+type_2[i][2]+"</option>");
                        } else {
                            $("#sel2").append("<option value="+type_2[i][1]+" >"+type_2[i][2]+"</option>");
                        }
                       
                    } else {
                        $("#sel2").append("<option value="+type_2[i][1]+">"+type_2[i][2]+"</option>");
                    }
                }
            }
            edit++;
            //
            $("#sort_msg1").html($("#sel1").find("option:selected").text());
        });
        $("#sel2").change(function(){
            var sel2_id=$(this).val();
            if(sel2_id==0){
                $("#sort_msg2").html("");
                return false;
            }
            $("#sort_msg2").html("-->"+$("#sel2").find("option:selected").text());
        });';
        if ($params['id'] > 0) {
            $con.= 'edit = 1;
            $("#sel1").change();';
        if ($params['stype'] > 0) {
            $con.='$("#sel2").change();';
            } 
       }
    $con.='});})
</script>';
    return $con;
}
?>