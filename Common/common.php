<?php

 if(!defined('TYPE_1'))
{
    define('TYPE_1','校区动态');
}
 if(!defined('TYPE_2'))
{
    define('TYPE_2','规章制度');
}
 if(!defined('TYPE_3'))
{
    define('TYPE_3','服务指南');
}
 if(!defined('TYPE_4'))
{
    define('TYPE_4','信息公告 ');
}
 if(!defined('TYPE_5'))
{
    define('TYPE_5','建议反馈 ');
}
 if(!defined('TYPE_6'))
{
    define('TYPE_6','工作研究 ');
}
 if(!defined('TYPE_7'))
{
    define('TYPE_7','资料下载 ');
}
function Gettypes($type){
    switch($type)
     {
         case "1":$ok=TYPE_1;break;
         case "2":$ok=TYPE_2; break;
         case "3":$ok=TYPE_3;break;
         case "4":$ok=TYPE_4;break;
         case "5":$ok=TYPE_5;break;
         case "6":$ok=TYPE_6;break;
         case "7":$ok=TYPE_7;break;
         default:$ok='ERROR';
     }
     return $ok;
}

if(!defined('IMAGE_1'))
{
    define('IMAGE_1','Up Image');
}
 if(!defined('IMAGE_2'))
{
    define('IMAGE_2','Down Image');
}
function GetImageTypes($type){
	switch($type)
	 {
	 	case "1":$ok=IMAGE_1;break;
        case "2":$ok=IMAGE_2; break;
        default:$ok='ERROR';
	 }
	 return $ok;
}

if(!defined('IMAGE_CLASS_1'))
{
    define('IMAGE_CLASS_1','校区风光');
}
 if(!defined('IMAGE_CLASS_2'))
{
    define('IMAGE_CLASS_2','四季风光');
}
 if(!defined('IMAGE_CLASS_3'))
{
    define('IMAGE_CLASS_3','银杏黄时');
}
 if(!defined('IMAGE_CLASS_4'))
{
    define('IMAGE_CLASS_4','其他');
}
function GetImageClass($type){
    switch($type)
     {
        case "1":$ok=IMAGE_CLASS_1;break;
        case "2":$ok=IMAGE_CLASS_2;break;
        case "3":$ok=IMAGE_CLASS_3;break;
        case "4":$ok=IMAGE_CLASS_4;break;
        default:$ok='ERROR';
     }
     return $ok;
}

if(!defined('ORDER_1'))
{
    define('ORDER_1','上午');
}
 if(!defined('ORDER_2'))
{
    define('ORDER_2','下午');
}
function GetOrderSel($type){
    switch($type)
     {
        case "1":$ok=ORDER_1;break;
        case "2":$ok=ORDER_2; break;
        default:$ok='ERROR';
     }
     return $ok;
}

if(!defined('VIDEO_0'))
{
    define('VIDEO_0','否');
}
 if(!defined('VIDEO_1'))
{
    define('VIDEO_1','是');
}
function GetVideoSel($type){
    switch($type)
     {
        case "0":$ok=VIDEO_0;break;
        case "1":$ok=VIDEO_1; break;
        default:$ok='ERROR';
     }
     return $ok;
}

if(!defined('ORDER_TIME_1'))
{
    define('ORDER_TIME_1','8:30~12:00');
}
 if(!defined('ORDER_TIME_2'))
{
    define('ORDER_TIME_2','8:30~12:00');
}
if(!defined('ORDER_TIME_3'))
{
    define('ORDER_TIME_3','8:30~12:00');
}
 if(!defined('ORDER_TIME_4'))
{
    define('ORDER_TIME_4','14:30~18:00');
}
if(!defined('ORDER_TIME_5'))
{
    define('ORDER_TIME_5','14:30~18:00');
}
 if(!defined('ORDER_TIME_6'))
{
    define('ORDER_TIME_6','14:30~18:00');
}
function GetOrderTime($type){
    switch($type)
     {
        case "1":$ok=ORDER_TIME_1;break;
        case "2":$ok=ORDER_TIME_2;break;
		case "3":$ok=ORDER_TIME_3;break;
		case "4":$ok=ORDER_TIME_4;break;
		case "5":$ok=ORDER_TIME_5;break;
		case "6":$ok=ORDER_TIME_6;break;
        default:$ok='ERROR';
     }
     return $ok;
}

if(!defined('DESCRIPT_1'))
{
    define('DESCRIPT_1','小于20人');
}
 if(!defined('DESCRIPT_2'))
{
    define('DESCRIPT_2','20到30人');
}
 if(!defined('DESCRIPT_3'))
{
    define('DESCRIPT_3','30到40人');
}
function GetDescript($type){
    switch($type)
     {
        case "1":$ok=DESCRIPT_1;break;
        case "2":$ok=DESCRIPT_2;break;
		case "3":$ok=DESCRIPT_3;break;
        default:$ok='ERROR';
     }
     return $ok;
}

if(!defined('SERVICE_1'))
{
    define('SERVICE_1','老师');
}
 if(!defined('SERVICE_2'))
{
    define('SERVICE_2','学生');
}
function GetServTypes($type){
	switch($type)
	 {
	 	case "1":$ok=SERVICE_1;break;
        case "2":$ok=SERVICE_2; break;
        default:$ok='ERROR';
	 }
	 return $ok;
}
/**
 * 定义分页
 */


function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=true)
{
    if(function_exists("mb_substr")) {
        if($suffix)
        {
            if($str==mb_substr($str, $start, $length, $charset))
            {
                return mb_substr($str, $start, $length, $charset);
            }
            else
            {
                return mb_substr($str, $start, $length, $charset)."...";
            }
        }
        else
        {
            return mb_substr($str, $start, $length, $charset);
        }
    }
    elseif(function_exists('iconv_substr')) {
        if($suffix)
        {
            if($str==iconv_substr($str,$start,$length,$charset))
            {
                return iconv_substr($str,$start,$length,$charset);
            }
            else
            {
                return iconv_substr($str,$start,$length,$charset)."...";
            }
        }
        else
        {
            return iconv_substr($str,$start,$length,$charset);
        }
    }
    $re['utf-8']   = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
    $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
    $re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
    $re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
    preg_match_all($re[$charset], $str, $match);
    $slice = join("",array_slice($match[0], $start, $length));
    if($suffix) return $slice."…";
    return $slice;
}

/**
 * 高亮显示搜索结果
 */
 function HighLightKeyWord($keyWord, $oldStr)
    {
    	$newStr = $oldStr;
    	strlen($keyWord) > 0 && $newStr = preg_replace("/".$keyWord."/i", "<b><font color='red'>".$keyWord."</font></b>", $oldStr);
    	return $newStr;
   }
/**
* 是否加了http://
*/
function urlFormat($url = null){
    if($url != null){
        if(!preg_match("/^(http|https)/i",$url)){
            $url ="http://" .$url;
        }
        return $url;
    }
    else{
        return "";
    }
}
/**
 * 过滤特殊字符
 * Enter description here ...
 * @param unknown_type $data
 */
function filterChar($data)
{
    if(is_array($data)){

        foreach($data as $key=>$value)
        {
            if(!is_array($value))
            {
                $data[$key] = htmlentities($value,ENT_COMPAT,'UTF-8');
            }
            else
                $this->addslash($data[$key]);
        }
    }
    else
    {
        $data = htmlentities($data,ENT_COMPAT,'UTF-8');
    }
    return $data;
}
/**
 * 输出时解析,高级表单编辑器
 */
function uh($str)
{
    $str = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $str);
   	$farr = array(
        "/\s+/",//过滤多余的空白
        "/<(\/?)(script|i?frame|style|html|body|title|link|meta|\?|\%)([^>]*?)>/isU",//过滤 <script 等可能引入恶意内容或恶意改变显示布局的代码,如果不需要插入flash等,还可以加入<object的过滤
        "/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",//过滤JavaScript的on事件
    );
    $tarr = array(
        " ",
        "", //如果要直接清除不安全的标签，这里可以留空
        "",
    );
    $str = preg_replace($farr,$tarr,$str);
    $str = htmlspecialchars($str);
    return $str;
}
/**
 +----------------------------------------------------------
 * 去掉对特殊字符的转意，输入是数组
 +----------------------------------------------------------
 * @参数 array $array 引用传递
 +----------------------------------------------------------
 * @return void
 +----------------------------------------------------------
 */
function StripS(&$array)
{
	foreach($array as $key=>$value){
		if(!is_array($array[$key])){
			$array[$key] = stripslashes($value);
		}
		else{
			StripS($array[$key]);
		}
	}
    return $array;
}
/**
 * 获得用户ip
 */
function getIp(){
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ip = $_SERVER['REMOTE_ADDR'];
    return $ip;
}
/**
 +----------------------------------------------------------
 * 隐藏十进制ip最后一段，变为*号
 +----------------------------------------------------------
 * @参数 string $ip
 +----------------------------------------------------------
 * @return string
 +----------------------------------------------------------
 */
function HiddenLastIP($ip)
{
	return preg_replace("/\.[\d]+$/", ".*", $ip);
}
/**
 * 读取换行空格
 */
function htmtocode($content){
   // $content = str_replace(" ","&nbsp;",$content);
    return $content;
}
/**
 * 表单验证函数
 */
function checkFixedphone($str) {
    $pattern = "/^(0)(\d{2}|\d{3})-(\d{7}|\d{8})$/i";
    return preg_match($pattern,$str);
}
function checkMobile($str) {
    $pattern = "/^(1)\d{10}$/i";
    return preg_match($pattern,$str);
}
function checkEmail($str) {
    $pattern = "/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/i";
    return preg_match($pattern,$str);
}
//
 function upload(){
        $UploadedFile=$_FILES['image']['tmp_name'];



          if($UploadedFile){
            import("ORG.Net.UploadFile");
            $upload = new UploadFile(); // 实例化上传类
            $upload->maxSize  = 314572811 ; // 设置附件上传大小
            $upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg','wma','mp3'); // 设置附件上传类型
            //$upload->allowTypes =   array('img','audio');//允许上传的文件类型（留空为不限制），使用数组设置，默认为空数组
            $upload->savePath =  './Uploadss/'; // 设置附件上传目录
            $upload->saveRule= com_create_guid;//多个文件是只能用uniqid或者com_create_guid，time会出现重复
            //上传文件的保存规则，必须是一个无需任何参数的函数名，例如可以是 time、 uniqid com_create_guid 等，但必须能保证生成的文件名是唯一的，默认是uniqid
            if(!$upload->upload()) {
                print_r($upload->getErrorMsg()) ;// 上传错误 提示错误信息
           //$this->error($upload->getErrorMsg());

            }else{ // 上传成功 获取上传文件信息

            $info =  $upload->getUploadFileInfo();
            dump($info) ;
         }
        }
        exit;
        $this->assign('inm',$info[0]["savename"]);
        $this->display();
    }
//
 /**
     * @author zzq
     * @brief 插件方法 
     */
function helper($funName, $params = array()) {
    $funName  = $funName.'_helper';
    $filePath = MYROOT . '/my/plugin/' . $funName . '.php';
    if (file_exists($filePath)) {
        include_once($filePath);
        if (function_exists($funName)) {
            return $funName($params);
        }
    }
}
?>