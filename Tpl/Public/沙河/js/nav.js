var timeout         = 300;
var closetimer		= 0;
var ddmenuitem      = 0;

function jsnav_open()
{	jsnav_canceltimer();
	jsnav_close();
	ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');}

function jsnav_close()
{	if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

function jsnav_timer()
{	closetimer = window.setTimeout(jsnav_close, timeout);}

function jsnav_canceltimer()
{	if(closetimer)
	{	window.clearTimeout(closetimer);
		closetimer = null;}}

$(document).ready(function()
{	$("#jsnav>li").bind('mouseover', jsnav_open);
	$("#jsnav>li").bind('mouseout',  jsnav_timer);});

document.onclick = jsnav_close;