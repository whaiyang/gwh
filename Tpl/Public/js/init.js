$(document).ready(function(){

    // 幻灯片组件初始化
    $('#slides').slides({
    	preload: true,
    	preloadImage: 'img/loading.gif',
    	play: 4000,
    	pause: 2500,
    	hoverPause: true,
    	pagination: true,
    	paginationClass: 'pagination',
    	slidesLoaded: function() {
    		$('.caption').animate({
    			bottom:0
    		},200);
    	}
    });   

    // menu控制  
    var $menu_list = $('#menu>li ul');
    $menu_list.each(function(i) {
      // console.log($(this).parent().width());
      $(this).width($(this).parent().outerWidth());
    });

    // 院校下拉框链接控制
    $('#footer .row-1 select').each(function(i) {
        $(this).change(function() {
            window.location.href = $(this).children('option:selected').val();
        });
    });

    // 实时时间显示
    (function($, win) {
    	
    	var date = new Date(),
    		$m_con = $('.month', '#block-feature'),
    		$d_con = $('.day', '#block-feature'),
    		$w_con = $('#week-inside'),
    		$t_con = $('#time-inside'),
    		mon_arr = ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '十二'],
    		week_arr = ['日', '一', '二', '三', '四', '五', '六'];

    	$m_con.text(mon_arr[date.getMonth()] + '月');
    	$d_con.text(date.getDate());
    	$w_con.text('星期' + week_arr[date.getDay()]);
    	$t_con.text(date.toString().substr(new Date().toString().indexOf(':') - 2, 5));
    	setInterval(function () {
    		$t_con.text(new Date().toString().substr(new Date().toString().indexOf(':') - 2, 5));
    	}, 60000);

    })(jQuery, window);

    // $(".item-tips").tooltip();

});

