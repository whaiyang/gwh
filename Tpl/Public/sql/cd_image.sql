-- phpMyAdmin SQL Dump
-- version 2.11.2.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 03 月 08 日 12:04
-- 服务器版本: 5.0.45
-- PHP 版本: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- 数据库: `manage`
--

-- --------------------------------------------------------

--
-- 表的结构 `cd_image`
--

CREATE TABLE `cd_image` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `link` varchar(255) character set utf8 NOT NULL COMMENT '图片链接',
  `title` varchar(255) character set utf8 NOT NULL COMMENT '图片说明',
  `image` varchar(255) character set utf8 NOT NULL COMMENT '图片地址',
  `thumb` varchar(100) character set utf8 NOT NULL COMMENT '缩微图',
  `comment` int(11) NOT NULL default '0' COMMENT '评论数目',
  `up` int(11) NOT NULL default '0' COMMENT '赞',
  `down` int(11) NOT NULL default '0',
  `time` int(11) NOT NULL,
  `type` int(2) NOT NULL,
  `class` int(2) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=95 ;

--
-- 导出表中的数据 `cd_image`
--

INSERT INTO `cd_image` (`id`, `user_id`, `link`, `title`, `image`, `thumb`, `comment`, `up`, `down`, `time`, `type`, `class`) VALUES
(23, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=113', '电子科大SAP教育中心及联合实验室成立', './uploads/2012-11/1352089051101451.jpg', './uploads/2012-11/thumb-1352089051101451.jpg', 0, 0, 0, 1352089053, 1, 0),
(35, 1, '', '', './uploads/2012-11/1352093582223670.jpg', './uploads/2012-11/thumb-1352093582223670.jpg', 0, 0, 0, 1352093585, 2, 1),
(34, 1, '', '', './uploads/2012-11/1352093567822498.jpg', './uploads/2012-11/thumb-1352093567822498.jpg', 0, 0, 0, 1352093570, 2, 2),
(33, 1, '', '', './uploads/2012-11/1352093554511417.jpg', './uploads/2012-11/thumb-1352093554511417.jpg', 0, 0, 0, 1352093557, 2, 3),
(26, 1, '', '', './uploads/2012-11/1352093308564340.jpg', './uploads/2012-11/thumb-1352093308564340.jpg', 0, 0, 0, 1352093312, 2, 4),
(27, 1, '', '', './uploads/2012-11/1352093341146567.jpg', './uploads/2012-11/thumb-1352093341146567.jpg', 0, 0, 0, 1352093345, 2, 2),
(28, 1, '', '', './uploads/2012-11/1352093389590483.jpg', './uploads/2012-11/thumb-1352093389590483.jpg', 0, 0, 0, 1352093392, 2, 1),
(29, 1, '', '', './uploads/2012-11/1352093429210836.jpg', './uploads/2012-11/thumb-1352093429210836.jpg', 0, 0, 0, 1352093431, 2, 3),
(30, 1, '', '', './uploads/2012-11/1352093447801179.jpg', './uploads/2012-11/thumb-1352093447801179.jpg', 0, 0, 0, 1352093450, 2, 1),
(31, 1, '', '', './uploads/2012-11/1352093465474288.jpg', './uploads/2012-11/thumb-1352093465474288.jpg', 0, 0, 0, 1352093468, 2, 2),
(24, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=115', '沙河校区管理委员会调研生命、经管学院', './uploads/2012-11/1352089469808111.jpg', './uploads/2012-11/thumb-1352089469808111.jpg', 0, 0, 0, 1352089489, 1, 0),
(32, 1, '', '', './uploads/2012-11/1352093541461354.jpg', './uploads/2012-11/thumb-1352093541461354.jpg', 0, 0, 0, 1352093544, 2, 0),
(20, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=108', '马争副校长一行赴北京部分高校调研', './uploads/2012-11/1352087652794144.jpg', './uploads/2012-11/thumb-1352087652794144.jpg', 0, 0, 0, 1352087673, 1, 0),
(21, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=109', '学校举行第三次校领导接待日', './uploads/2012-11/1352087940650446.jpg', './uploads/2012-11/thumb-1352087940650446.jpg', 0, 0, 0, 1352087959, 1, 0),
(22, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=112', '第12届IEEE计算机与信息技术国际会议在我校举行', './uploads/2012-11/1352088760387907.jpg', './uploads/2012-11/thumb-1352088760387907.jpg', 0, 0, 0, 1352088762, 1, 0),
(36, 1, '', '', './uploads/2012-11/1352093613431789.jpg', './uploads/2012-11/thumb-1352093613431789.jpg', 0, 0, 0, 1352093616, 2, 0),
(37, 1, '', '', './uploads/2012-11/1352093631799433.jpg', './uploads/2012-11/thumb-1352093631799433.jpg', 0, 0, 0, 1352093634, 2, 0),
(38, 1, '', '', './uploads/2012-11/1352093645282995.jpg', './uploads/2012-11/thumb-1352093645282995.jpg', 0, 0, 0, 1352093647, 2, 0),
(39, 1, '', '', './uploads/2012-11/1352093663886914.jpg', './uploads/2012-11/thumb-1352093663886914.jpg', 0, 0, 0, 1352093666, 2, 0),
(40, 1, '', '', './uploads/2012-11/1352093678173045.jpg', './uploads/2012-11/thumb-1352093678173045.jpg', 0, 0, 0, 1352093683, 2, 0),
(41, 1, '', '', './uploads/2012-11/1352093696581020.jpg', './uploads/2012-11/thumb-1352093696581020.jpg', 0, 0, 0, 1352093699, 2, 0),
(42, 1, '', '', './uploads/2012-11/1352093715596539.jpg', './uploads/2012-11/thumb-1352093715596539.jpg', 0, 0, 0, 1352093718, 2, 0),
(43, 1, '', '', './uploads/2012-11/1352093730984809.jpg', './uploads/2012-11/thumb-1352093730984809.jpg', 0, 0, 0, 1352093732, 2, 0),
(44, 1, '', '', './uploads/2012-11/1352093741377798.jpg', './uploads/2012-11/thumb-1352093741377798.jpg', 0, 0, 0, 1352093744, 2, 0),
(45, 1, '', '', './uploads/2012-11/1352093755115944.jpg', './uploads/2012-11/thumb-1352093755115944.jpg', 0, 0, 0, 1352093758, 2, 0),
(46, 1, '', '', './uploads/2012-11/1352093783961138.jpg', './uploads/2012-11/thumb-1352093783961138.jpg', 0, 0, 0, 1352093786, 2, 0),
(47, 1, '', '', './uploads/2012-11/1352093798555184.jpg', './uploads/2012-11/thumb-1352093798555184.jpg', 0, 1, 0, 1352093801, 2, 0),
(48, 1, '', '', './uploads/2012-11/1352093839235750.jpg', './uploads/2012-11/thumb-1352093839235750.jpg', 0, 1, 0, 1352093843, 2, 0),
(49, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=116', '第五届研究生青春风采大赛初赛精彩上演', './uploads/2012-11/1352094294679387.jpg', './uploads/2012-11/thumb-1352094294679387.jpg', 0, 0, 0, 1352094314, 1, 0),
(50, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=174', '校排舞运动队获全国排舞大赛一等奖', './uploads/2012-11/1352257995816830.jpg', './uploads/2012-11/thumb-1352257995816830.jpg', 0, 0, 0, 1352258015, 1, 0),
(51, 1, '', '', './uploads/2012-11/1352853652835885.jpg', './uploads/2012-11/thumb-1352853652835885.jpg', 0, 0, 0, 1352853655, 2, 0),
(52, 1, '', '', './uploads/2012-11/1352853691181251.jpg', './uploads/2012-11/thumb-1352853691181251.jpg', 0, 0, 0, 1352853700, 2, 0),
(53, 1, '', '', './uploads/2012-11/1352853839573252.jpg', './uploads/2012-11/thumb-1352853839573252.jpg', 0, 0, 0, 1352853841, 2, 0),
(55, 1, '', '', './uploads/2012-11/1352853936535922.jpg', './uploads/2012-11/thumb-1352853936535922.jpg', 0, 0, 0, 1352853940, 2, 0),
(68, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=194', '沙河校区首届银杏节开幕', './uploads/2012-11/1353900043871414.jpg', './uploads/2012-11/thumb-1353900043871414.jpg', 0, 0, 0, 1353900060, 1, 0),
(57, 1, '', '', './uploads/2012-11/1352854101655024.jpg', './uploads/2012-11/thumb-1352854101655024.jpg', 0, 0, 0, 1352854102, 2, 0),
(58, 1, '', '', './uploads/2012-11/1352854114641534.jpg', './uploads/2012-11/thumb-1352854114641534.jpg', 0, 0, 0, 1352854115, 2, 0),
(59, 1, '', '', './uploads/2012-11/1352854235675744.jpg', './uploads/2012-11/thumb-1352854235675744.jpg', 0, 0, 0, 1352854236, 2, 0),
(60, 1, '', '', './uploads/2012-11/1352854247123042.jpg', './uploads/2012-11/thumb-1352854247123042.jpg', 0, 0, 0, 1352854249, 2, 0),
(62, 1, '', '', './uploads/2012-11/1353295503274415.jpg', './uploads/2012-11/thumb-1353295503274415.jpg', 0, 0, 0, 1353295505, 2, 0),
(63, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=186', '太赫兹科学协同创新中心第一届工作会议召开', './uploads/2012-11/1353317678536028.jpg', './uploads/2012-11/thumb-1353317678536028.jpg', 0, 0, 0, 1353317704, 1, 0),
(64, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=189', '我校举办2012应急管理论坛', './uploads/2012-11/1353476754296353.jpg', './uploads/2012-11/thumb-1353476754296353.jpg', 0, 0, 0, 1353476772, 1, 0),
(65, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=190', '学校举行报告会学习传达十八大精神', './uploads/2012-11/1353548428610716.jpg', './uploads/2012-11/thumb-1353548428610716.jpg', 0, 0, 0, 1353548461, 1, 0),
(66, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=191', '2012年全国博士生学术论坛在我校举行', './uploads/2012-11/1353552527198612.jpg', './uploads/2012-11/thumb-1353552527198612.jpg', 0, 0, 0, 1353552542, 1, 0),
(67, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=192', '学校召开2012年新进教职工暨新任科级干部培训会', './uploads/2012-11/1353638992976872.jpg', './uploads/2012-11/thumb-1353638992976872.jpg', 0, 0, 0, 1353639025, 1, 0),
(69, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=195', '马争副校长赴华南理工大学调研', './uploads/2012-11/1353900599856592.jpg', './uploads/2012-11/thumb-1353900599856592.jpg', 0, 0, 0, 1353900621, 1, 0),
(70, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=202', '高校马克思主义“三化”理论研究座谈会在我校举行', './uploads/2012-11/1354246375427206.jpg', './uploads/2012-11/thumb-1354246375427206.jpg', 0, 0, 0, 1354246393, 1, 0),
(71, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=205', '我校两国家重大专项课题召开验收会议', './uploads/2012-12/1354866457701825.jpg', './uploads/2012-12/thumb-1354866457701825.jpg', 0, 0, 0, 1354866474, 1, 0),
(72, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=207', '学校举办科大附小才艺作品展', './uploads/2012-12/1355127522727920.jpg', './uploads/2012-12/thumb-1355127522727920.jpg', 0, 0, 0, 1355127541, 1, 0),
(73, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=208', '省电子学会半导体与集成技术专委会2012年年会在我校举行', './uploads/2012-12/1355127727270830.jpg', './uploads/2012-12/thumb-1355127727270830.jpg', 0, 0, 0, 1355127748, 1, 0),
(76, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=209', '学校举办“留学生中国文化才艺展”', './uploads/2012-12/1355131030779992.jpg', './uploads/2012-12/thumb-1355131030779992.jpg', 0, 0, 0, 1355131047, 1, 0),
(75, 1, 'http://www.news.uestc.edu.cn/NewsRead.aspx?newsID=56358', '出版社举办“银杏节”图书展 ', './uploads/2012-12/1355130851617756.jpg', './uploads/2012-12/thumb-1355130851617756.jpg', 0, 0, 0, 1355130867, 1, 0),
(77, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=211', '学校网络教育学院举办十周年庆典', './uploads/2012-12/1355194632830978.jpg', './uploads/2012-12/thumb-1355194632830978.jpg', 0, 0, 0, 1355194651, 1, 0),
(79, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=217', '电子薄膜与集成器件国家重点实验室召开学术委员会年度会议', './uploads/2012-12/1355731171592452.jpg', './uploads/2012-12/thumb-1355731171592452.jpg', 0, 0, 0, 1355731189, 1, 0),
(80, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=219', '我校第九届IMBA举行毕业典礼', './uploads/2012-12/1355793293783573.jpg', './uploads/2012-12/thumb-1355793293783573.jpg', 0, 0, 0, 1355793309, 1, 0),
(81, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=220', '四川省高校心理健康教育工作年会在我校召开', './uploads/2012-12/1355877783642561.jpg', './uploads/2012-12/thumb-1355877783642561.jpg', 0, 0, 0, 1355877805, 1, 0),
(82, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=221', '四川省认知科学学会成立仪式在我校举行', './uploads/2012-12/1356326392856986.jpg', './uploads/2012-12/thumb-1356326392856986.jpg', 0, 0, 0, 1356326408, 1, 0),
(83, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=233', '信息与软件工程学院举行2013迎新晚会', './uploads/2013-01/1357267784539652.jpg', './uploads/2013-01/thumb-1357267784539652.jpg', 0, 0, 0, 1357267812, 1, 0),
(84, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=235', 'ACM Chengdu Chapter成立大会在我校召开', './uploads/2013-01/1357365042555001.jpg', './uploads/2013-01/thumb-1357365042555001.jpg', 0, 0, 0, 1357365067, 1, 0),
(85, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=237', '中国西部信用风险分析与管理论坛在我校举行', './uploads/2013-01/1357538759635979.jpg', './uploads/2013-01/thumb-1357538759635979.jpg', 0, 0, 0, 1357538772, 1, 0),
(86, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=242', '经管学院聘请EMBA校友导师', './uploads/2013-01/1357611895254804.jpg', './uploads/2013-01/thumb-1357611895254804.jpg', 0, 0, 0, 1357611912, 1, 0),
(87, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=243', 'IEEE ICWAMTIP2012国际会议在我校举行', './uploads/2013-01/1357808405740392.jpg', './uploads/2013-01/thumb-1357808405740392.jpg', 0, 0, 0, 1357808408, 1, 0),
(88, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=247', '学校召开校领导述职与民主测评大会', './uploads/2013-01/1358299341240025.jpg', './uploads/2013-01/thumb-1358299341240025.jpg', 0, 0, 0, 1358299362, 1, 0),
(89, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=251', '学校开展“写春联送祝福”活动', './uploads/2013-01/1358409566560639.jpg', './uploads/2013-01/thumb-1358409566560639.jpg', 0, 0, 0, 1358409586, 1, 0),
(90, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=252', '学校举行民主党派总结表彰大会', './uploads/2013-01/1358487225526544.jpg', './uploads/2013-01/thumb-1358487225526544.jpg', 0, 0, 0, 1358487245, 1, 0),
(91, 1, 'http://gwh.uestc.edu.cn/gwh/index.php/Publication/detail?id=256', '学校召开离退休教职工新春团拜会', './uploads/2013-01/1359333593820579.jpg', './uploads/2013-01/thumb-1359333593820579.jpg', 0, 0, 0, 1359333609, 1, 0),
(92, 1, '', '阿飞是否规范和官方电话', '&lt;br /&gt;\r\n&lt;b&gt;Fatal error&lt;/b&gt;:  Call to undefined function ImageCreateFromjpeg() in &lt;b&gt;E:\\\\wamp\\\\www\\\\gwh\\\\Lib\\\\Action\\\\ImageAction.class.php&lt;/b&gt; on line &lt;b&gt;54&lt;/b&gt;&lt;br /&gt;\r\n', '&lt;br /&gt;\r\n&lt;b&gt;Fatal error&lt;/b&gt;:  Call to undefined function ImageCreateFromjpeg() in &', 0, 0, 0, 1361501518, 2, 0),
(93, 1, '', '三巨头', './uploads/2013-02/1361505162800206.jpg', './uploads/2013-02/thumb-1361505162800206.jpg', 0, 0, 0, 1361505185, 2, 0),
(94, 1, '', '这地方政府', './uploads/2013-02/1361505178118408.jpg', './uploads/2013-02/thumb-1361505178118408.jpg', 0, 0, 0, 1361505185, 2, 0);
