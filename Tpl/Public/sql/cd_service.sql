-- phpMyAdmin SQL Dump
-- version 2.11.2.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 11 月 14 日 04:11
-- 服务器版本: 5.0.45
-- PHP 版本: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- 数据库: `manage`
--

-- --------------------------------------------------------

--
-- 表的结构 `cd_service`
--

CREATE TABLE `cd_service` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) character set utf8 NOT NULL,
  `link` varchar(255) character set utf8 NOT NULL,
  `time` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1:老师，2:学生',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- 导出表中的数据 `cd_service`
--

INSERT INTO `cd_service` (`id`, `title`, `link`, `time`, `type`) VALUES
(1, 'gfh', 'http://fhjkjhjljkjk', 1352863184, 1),
(2, 'dghfghfg', 'http://sghdfhgd', 0, 2);
