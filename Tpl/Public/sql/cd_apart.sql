-- phpMyAdmin SQL Dump
-- version 2.11.2.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 02 月 04 日 15:05
-- 服务器版本: 5.0.45
-- PHP 版本: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- 数据库: `manage`
--

-- --------------------------------------------------------

--
-- 表的结构 `cd_apart`
--

CREATE TABLE `cd_apart` (
  `id` int(11) NOT NULL auto_increment,
  `apart_name` varchar(255) NOT NULL COMMENT '部门名称',
  `apart_user` varchar(255) NOT NULL COMMENT '部门用户名',
  `apart_passwd` varchar(255) NOT NULL COMMENT '密码',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 导出表中的数据 `cd_apart`
--

INSERT INTO `cd_apart` (`id`, `apart_name`, `apart_user`, `apart_passwd`) VALUES
(1, '管委会', 'gwh', '123456'),
(2, '学工部', 'xgb', '123456');
