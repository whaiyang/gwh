<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="电子科技大学沙河校区" />
	<meta name="keywords" content="uestc, 电子科技大学, 沙河校区">
	<title>电子科技大学沙河校区</title>
	<link rel="stylesheet" type="text/css" href="../Public/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="../Public/css/style.css">
	<link rel="stylesheet" type="text/css" href="../Public/css/main.css">
	<script type="text/javascript" src="../Public/js/jquery.min.js"></script>
	<script type="text/javascript" src="../Public/js/addload.js"></script>
	<script type="text/javascript" src="../Public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../Public/js/slides.min.jquery.js"></script>
	<script type="text/javascript" src="../Public/js/init.js"></script>
	<script type="text/javascript" charset="utf-8" src="../Public/js/scrolltopcontrol.js"></script>
</head>
<body id="body">
	<div class="container">
		<div id="header">
			<div id="logo">
				<div class="flag">
					<a href="__APP__"><img src="../Public/images/logo.png"></a>
				</div>
				<div class="campus">
					<a href="__APP__"><img src="../Public/images/shahe_campus.png"></a>
				</div>
				<div class="search">
					<div class="search-bar pull-right">
						<div class="input-append">
							<input class="span2 search-content" type="text" />
							<button class="btn btn-info btn-search"><span class="icon-search"></span></button>	
						</div> 
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="func">
					<ul class="func-bar pull-right">
						<li><img src="../Public/images/home.png"><a href="http://www.uestc.edu.cn/">学校主页</a></li>
						<li><img src="../Public/images/study.png"><a href="__APP__/Publication/index?type=5">工作研究</a></li>
						<li><img src="../Public/images/admin.png"><a href="#admin-login" data-toggle="modal">管理登入</a></li>
						<li><img src="../Public/images/visit.png"><a href="#">总访问量&nbsp;&nbsp;<span><?php echo $vister;?></span></a></li>
					</ul>
				</div>
				<div id="admin-login" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4>管理登入</h4>
					</div>
					<div class="modal-body">
						<form method="post" name="myform" action="__APP__/Login/checkAdmin">
							<label>用户名：</label>
							<input type="text" name="username">
							<label>密  码：</label>
							<input type="password" name="password" style="display:block;">
							<button class="btn btn-primary">登陆</button>
						</form>
					</div>
				</div>
			</div>
			<div id="nav">
				<ul id="menu">
					<!-- <li><a href="#">校区总览</a>
						<ul>
							<li><a href="__APP__/Publication/detail?id=1">校区概况</a></li>
							<li><a href="__APP__/Publication/detail?id=2">管理机构</a></li>
							<li><a href="#">常驻学院</a></li>
							<li><a href="#">部门黄页</a></li>
							<li><a href="#">魅力沙河</a></li>
						</ul>
					</li>
					<li><a href="__APP__/Publication/index?type=2">规章制度</a></li>
					<li><a href="#">公共服务</a>
						<ul>
							<li><a href="#">特色服务</a></li>
							<li><a href="#">实用信息</a></li>
							<li><a href="#">办事流程</a></li>
							<li><a href="__APP__/Download">资料下载</a></li>	
						</ul>
					</li>
					<li><a href="__APP__/Service/index">办事大厅</a></li> -->
					<?php foreach($menu['parent'] as $key=>$value){ ?>
							<li pid=<?=$value['id']?>> 
								<a href="<?=empty($value['link'])?(empty($value['article'])?"__APP__/Publication/index?type=".$value['board']:"__APP__/Publication/detail?id=".$value['article']):str_replace("[__APP__]",__APP__,$value['link'])?>"><?=$value['name']?></a>
								<ul style="left:0;">
								<?php foreach($menu['son'] as $key=>$value_son){ ?>
									<?php if($value_son['parent_id']==$value['id']){ ?>
										<li><a href="<?=empty($value_son['link'])?(empty($value_son['article'])?"__APP__/Publication/index?type=".$value_son['board']:"__APP__/Publication/detail?id=".$value_son['article']):str_replace("[__APP__]",__APP__,$value_son['link'])?>"><?=$value_son['name']?></a></li>
									<?php } ?>
								<?php } ?>
								</ul>
							</li>
						<?php } ?>
					
					<div class="clearfix"></div>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
  <div id="content">
    <script type="text/javascript" src="../Public/fancyBox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="../Public/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<link rel="stylesheet" type="text/css" href="../Public/fancyBox/source/jquery.fancybox.css"/>

<script type="text/javascript">
  $(document).ready(function() {
    $("a#singleImg").fancybox();
    var config = {
        'transitionIn'  :   'elastic',
        'transitionOut' :   'elastic',
        'speedIn'       :   600,
        'speedOut'      :   200,
        'overlayShow'   :   false
    };
    $("a.group1").fancybox(config);
    $("a.group2").fancybox(config);
    $("a.group3").fancybox(config);
  })  
</script>

<style type="text/css">
  .imgContent
  {
    margin-top: 10px;
    width:159;
    border: solid #eee 2px;
  }

  .imgContent img
  {
    height:86px;
    width:114px;
  }

  .meeting_desc
  {
    margin-top: 20px;
    margin-bottom: 20px;
    border: solid #eee 2px;
  }
</style>
<div id="leftdiv" class="corner" style="height:551px;width:260px;overflow-y:scroll;overflow-x:hidden;">
      <div id="marquee" style="width:238px;">
          <div class="part">
            <div class="meeting_desc">
              <h1>会议室322</h1>
              <p>
                最大容量： 35人</br>
                提供设备：投影幕布，茶水，纸杯</br>
                悬挂横幅尺寸：</br>
                &emsp;&emsp;&emsp;&emsp;① 靠窗一侧  8.7m * 0.7m</br>
                &emsp;&emsp;&emsp;&emsp;② 幕布对面 （只能用透明胶固定）6.8m * 0.7m
              </p>
            </div>
            <div class="imgContent">
                <a rel="group1" id="singleImg" href="../Public/meetingImg/322/DSC_2828.JPG"><img src="../Public/meetingImg/322/DSC_2828.JPG"/></a>
                <a rel="group1" id="singleImg" href="../Public/meetingImg/322/DSC_2829.JPG"><img src="../Public/meetingImg/322/DSC_2829.JPG"/></a>
                <a rel="group1" rel="group1" id="singleImg" href="../Public/meetingImg/322/DSC_2834.JPG"><img src="../Public/meetingImg/322/DSC_2834.JPG"/></a>
                <a rel="group1" id="singleImg" href="../Public/meetingImg/322/DSC_2845.JPG"><img src="../Public/meetingImg/322/DSC_2845.JPG"/></a>
                <a rel="group1" id="singleImg" href="../Public/meetingImg/322/DSC_2847.JPG"><img src="../Public/meetingImg/322/DSC_2847.JPG"/></a>
            </div>
          </div>

          <div class="part">
            <div class="meeting_desc">
              <h1>会议室324</h1>
              <p>
                最大容量： 24人</br>
                提供设备：投影幕布，茶水，纸杯</br>
                悬挂横幅尺寸：</br>
                &emsp;&emsp;&emsp;&emsp;① 靠窗一侧  7.5m * 0.7m
              </p>
            </div>
            <div class="imgContent">
                <a  rel="group2" id="singleImg" href="../Public/meetingImg/324/DSC_2808.JPG"><img src="../Public/meetingImg/324/DSC_2808.JPG"/></a>
                <a  rel="group2" id="singleImg" href="../Public/meetingImg/324/DSC_2810.JPG"><img src="../Public/meetingImg/324/DSC_2810.JPG"/></a>
                <a  rel="group2" id="singleImg" href="../Public/meetingImg/324/DSC_2811.JPG"><img src="../Public/meetingImg/324/DSC_2811.JPG"/></a>
                <a  rel="group2" id="singleImg" href="../Public/meetingImg/324/DSC_2812.JPG"><img src="../Public/meetingImg/324/DSC_2812.JPG"/></a>
            </div>
          </div>

          <div class="part">
            <div class="meeting_desc">
              <h1>会议室335</h1>
              <p>
                最大容量： 45人</br>
                提供设备：视频会议系统，茶水，纸杯</br>
                悬挂横幅尺寸：</br>
                &emsp;&emsp;&emsp;&emsp;① 靠窗一侧  11.5m * 0.7m</br>
                &emsp;&emsp;&emsp;&emsp;② 幕布对面  6.5m * 0.7m
              </p>
            </div>
            <div class="imgContent">
                <a  rel="group3" id="singleImg" href="../Public/meetingImg/335/DSC_2783.JPG"><img src="../Public/meetingImg/335/DSC_2783.JPG"/></a>
                <a  rel="group3" id="singleImg" href="../Public/meetingImg/335/DSC_2785.JPG"><img src="../Public/meetingImg/335/DSC_2785.JPG"/></a>
                <a  rel="group3" id="singleImg" href="../Public/meetingImg/335/DSC_2786.JPG"><img src="../Public/meetingImg/335/DSC_2786.JPG"/></a>
                <a  rel="group3" id="singleImg" href="../Public/meetingImg/335/DSC_2791.JPG"><img src="../Public/meetingImg/335/DSC_2791.JPG"/></a>
                <a  rel="group3" id="singleImg" href="../Public/meetingImg/335/DSC_2793.JPG"><img src="../Public/meetingImg/335/DSC_2793.JPG"/></a>
            </div>
          </div>
      </div>
</div>
    <!--以下是右侧内容-->
    <div id="rightdiv" class="corner" style="width:661px;">
      <h1>会务预约</h1>
	  <div class="clear"></div>
	     <?php echo $str;?>
    </div>
	 <div class="clear"></div>
	</div>
  <div class="clear"></div>
 <?php echo W('ShowComment',array('count'=>5));?>
 <script> 
$(document).ready(function(){
    $(".example9").click(function(){
        $('#order_form input[type="reset"]').trigger('click');
        $('#vedio_select').css('display', 'none');
        var dateTime = $(this).parent().next(".more_inf").find(".order_starts").val();
        $("#order_start").val(dateTime);    
        $("#contentOrder").html($(this).parent().next(".more_inf").html());      
    });
    $('#order_room').change(function() {
        if ($(this).val() == '335') {
            $('#vedio_select').css('display', 'block');
        } else {
            $('#vedio_select').css('display', 'none');
        }
    });
 });
 </script>
 <script type="text/javascript">
 function checkForm()
 {
     if(document.getElementById('order_room').value==""){
        alert("请选择预约会议室");
        document.getElementById('order_room').focus();
        return false;
    }
    else if(document.getElementById('order_time').value==""){
        alert("请选择会议时段");
        document.getElementById('order_time').focus();
        return false;
    }
    else if(document.getElementById('descript').value==""){
        alert("请选择参会人数");
        document.getElementById('descript').focus();
        return false;
    }
    else if(document.getElementById('phone_num').value.length<8){
        alert("请填写正确联系方式");
        document.getElementById('phone_num').focus();
        return false;
    }
    $("#order_form").submit();
 }
 </script>
 <div id="footer_pg">
    <p>
    电子科技大学沙河校区管理委员会@版权所有<br/>沙河校区管理委员会办公室地址:成都市建设北路二段四号 沙河校区主楼中332室 邮编:610054<br/>联系电话：028-83201875
    </p>
  </div>
</body>
</html>