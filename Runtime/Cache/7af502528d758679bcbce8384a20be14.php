<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="电子科技大学沙河校区" />
	<meta name="keywords" content="uestc, 电子科技大学, 沙河校区">
	<title>电子科技大学沙河校区</title>
	<link rel="stylesheet" type="text/css" href="../Public/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="../Public/css/style.css">
	<link rel="stylesheet" type="text/css" href="../Public/css/main.css">
	<script type="text/javascript" src="../Public/js/jquery.min.js"></script>
	<script type="text/javascript" src="../Public/js/addload.js"></script>
	<script type="text/javascript" src="../Public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../Public/js/slides.min.jquery.js"></script>
	<script type="text/javascript" src="../Public/js/init.js"></script>
	<script type="text/javascript" charset="utf-8" src="../Public/js/scrolltopcontrol.js"></script>
</head>
<body id="body">
	<div class="container">
		<div id="header">
			<div id="logo">
				<div class="flag">
					<a href="__APP__"><img src="../Public/images/logo.png"></a>
				</div>
				<div class="campus">
					<a href="__APP__"><img src="../Public/images/shahe_campus.png"></a>
				</div>
				<div class="search">
					<div class="search-bar pull-right">
						<div class="input-append">
							<input class="span2 search-content" type="text" />
							<button class="btn btn-info btn-search"><span class="icon-search"></span></button>	
						</div> 
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="func">
					<ul class="func-bar pull-right">
						<li><img src="../Public/images/home.png"><a href="http://www.uestc.edu.cn/">学校主页</a></li>
						<li><img src="../Public/images/study.png"><a href="__APP__/Publication/index?type=5">工作研究</a></li>
						<li><img src="../Public/images/admin.png"><a href="#admin-login" data-toggle="modal">管理登入</a></li>
						<li><img src="../Public/images/visit.png"><a href="#">总访问量&nbsp;&nbsp;<span><?php echo $vister;?></span></a></li>
					</ul>
				</div>
				<div id="admin-login" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4>管理登入</h4>
					</div>
					<div class="modal-body">
						<form method="post" name="myform" action="__APP__/Login/checkAdmin">
							<label>用户名：</label>
							<input type="text" name="username">
							<label>密  码：</label>
							<input type="password" name="password" style="display:block;">
							<button class="btn btn-primary">登陆</button>
						</form>
					</div>
				</div>
			</div>
			<div id="nav">
				<ul id="menu">
					<!-- <li><a href="#">校区总览</a>
						<ul>
							<li><a href="__APP__/Publication/detail?id=1">校区概况</a></li>
							<li><a href="__APP__/Publication/detail?id=2">管理机构</a></li>
							<li><a href="#">常驻学院</a></li>
							<li><a href="#">部门黄页</a></li>
							<li><a href="#">魅力沙河</a></li>
						</ul>
					</li>
					<li><a href="__APP__/Publication/index?type=2">规章制度</a></li>
					<li><a href="#">公共服务</a>
						<ul>
							<li><a href="#">特色服务</a></li>
							<li><a href="#">实用信息</a></li>
							<li><a href="#">办事流程</a></li>
							<li><a href="__APP__/Download">资料下载</a></li>	
						</ul>
					</li>
					<li><a href="__APP__/Service/index">办事大厅</a></li> -->
					<?php foreach($menu['parent'] as $key=>$value){ ?>
							<li pid=<?=$value['id']?>> 
								<a href="<?=empty($value['link'])?(empty($value['article'])?"__APP__/Publication/index?type=".$value['board']:"__APP__/Publication/detail?id=".$value['article']):str_replace("[__APP__]",__APP__,$value['link'])?>"><?=$value['name']?></a>
								<ul style="left:0;">
								<?php foreach($menu['son'] as $key=>$value_son){ ?>
									<?php if($value_son['parent_id']==$value['id']){ ?>
										<li><a href="<?=empty($value_son['link'])?(empty($value_son['article'])?"__APP__/Publication/index?type=".$value_son['board']:"__APP__/Publication/detail?id=".$value_son['article']):str_replace("[__APP__]",__APP__,$value_son['link'])?>"><?=$value_son['name']?></a></li>
									<?php } ?>
								<?php } ?>
								</ul>
							</li>
						<?php } ?>
					
					<div class="clearfix"></div>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

<title>相册 | 相册列表</title>
<script type="text/javascript" src="../Public/js/highslide-full.min.js"></script>
<link rel="stylesheet" type="text/css" href="../Public/css/highslide.css" />
<link rel="stylesheet" rev="stylesheet" href="../Public/css/37.css" type="text/css" media="screen" id="g_style">
<link rel="stylesheet" rev="stylesheet" href="../Public/css/photo_v3.css" type="text/css" media="screen">
</head>
<style>
    .page_albumlist .photo_layout {
padding-left: 25px;
overflow: hidden;
}
a{
    text-decoration: none !important;
}
#nav {
    padding: 4px 149px;
}

</style>
<script type="text/javascript">
hs.graphicsDir = '../Public/css/graphics/';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.fadeInOut = true;
hs.dimmingOpacity = 0.8;
hs.outlineType = 'rounded-white';
hs.captionEval = 'this.thumb.alt';
hs.marginBottom = 105; // make room for the thumbstrip and the controls
hs.numberPosition = 'caption';

// Add the slideshow providing the controlbar and the thumbstrip
hs.addSlideshow({
	//slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	overlayOptions: {
		className: 'text-controls',
		position: 'bottom center',
		relativeTo: 'viewport',
		offsetY: -60
	},
	thumbstrip: {
		position: 'bottom center',
		mode: 'horizontal',
		relativeTo: 'viewport'
	}
});
</script>

<!--
<script>
$(function(){
    $(".praise").click(function(){
        var doms=$(this);
       var id=$(this).attr('id');
        $.get('__APP__/Ajax/imageUp',{id:id},function(data){
            if(data==0){
                alert('error');
            }else if(data==1){
                doms.next('span').html( parseInt(doms.next('span').html())+1 );
            }else{
                return false;
            }
        });
    });
})
</script>
-->
<body >
<div id="content" style="height: auto;">
    <div class="mod_wrap_inner">
        <div class="bg2 bor3 qz_tab_v2">
            <div class="bd">
                <div id="tab_nav" class="qz_tab_v2_nav">
                    <ul>
                        <li id="nav_album" class="">
                            <a id="nav_a_album" href="__URL__/image"  title="相册" class="c_tx2 tcisd_home bg bor3" tcisdkey="mine">相册</a>
                        </li>
                       
						<li id="nav_new" class="qzone-display">
                            <a id="nav_a_new" href="__URL__/imageList"  title="最新照片" class="c_tx2 ">最新照片</a>
                        </li>
                        <!-- 
						<li id="nav_marked" class="qzone-display">
                            <a id="nav_a_marked" href="#mod=marked"  title="被圈照片" class="c_tx2">被圈照片</a>
                        </li>
						<li id="nav_aboutme" class="" style="display:none">
                            <a id="nav_a_aboutme" href="#mod=aboutme"  title="关于我的照片" class="c_tx2">关于我的照片</a>
                        </li>
						<li id="nav_video" class="qzone-display">
                            <a id="nav_a_video" href="javascript:void(0);" title="我的视频" class="c_tx2">我的视频</a>
                        </li>
                         -->
                    </ul>
                </div>
                
            </div>
        </div> 
    </div>
<div class="">
    <div class="mod_wrap_bd">
      <div class="photo_v3 page_albumlist" id="app_mod">
      <div class="clear photo_layout">
      <div class="photo_layout_section" id="contianer_div" style="width: 860px; ">
         <div id="album_list_div" class="">
            <div class="clear photo_albumlist" id="album_list_div">
               <ul id="album_list_container">
<?php  foreach($result as $key => $value) { ?>
                 <li >
                     <div class="bor2 photo_albumlist_bg bg"></div>
                     <div class="photo_albumlist_wrap">
                         <div class="bd bg bor2">
                             <p class="photo_albumlist_img">
                                 <a href="__URL__/imageList?type=<?php echo $value['value'];?>" class="tcisd_albumlist" >
                                 <img src="__ROOT__/<?php if($value['other'] == ""){echo "uploads/default.jpeg";}else{ echo $value['other'];}?>" style="width: 180px; height: 135px; margin-left: 0; margin-top: 0px;"></a>
                             </p>
                         </div>
	                     <div class="ft">
	                        <p class="photo_albumlist_name">
	                           <a title="<?php echo $value['name'];?>" href="" class="c_tx2" ><?php echo $value['name'];?></a>
	                         </p>
	                         <p class="c_tx3 photo_albumlist_photocount" style=""><?php echo $value['count'];?>张&nbsp;    <span class="limit"></span></p>
	                     </div>
                     </div>
                 </li> 
<?php } ?>
               
                 
              </ul>
            </div>
         </div>







</div></div></div></div></div></div>
 <div id="footer_pg">
    <p>
    电子科技大学沙河校区管理委员会@版权所有<br/>沙河校区管理委员会办公室地址:成都市建设北路二段四号 沙河校区主楼中332室 邮编:610054<br/>联系电话：028-83201875
    </p>
  </div>
</body>
</html>