<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="电子科技大学沙河校区" />
	<meta name="keywords" content="uestc, 电子科技大学, 沙河校区">
	<title>电子科技大学沙河校区</title>
	<link rel="stylesheet" type="text/css" href="../Public/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="../Public/css/style.css">
	<link rel="stylesheet" type="text/css" href="../Public/css/main.css">
	<script type="text/javascript" src="../Public/js/jquery.min.js"></script>
	<script type="text/javascript" src="../Public/js/addload.js"></script>
	<script type="text/javascript" src="../Public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../Public/js/slides.min.jquery.js"></script>
	<script type="text/javascript" src="../Public/js/init.js"></script>
	<script type="text/javascript" charset="utf-8" src="../Public/js/scrolltopcontrol.js"></script>
</head>
<body id="body">
	<div class="container">
		<div id="header">
			<div id="logo">
				<div class="flag">
					<a href="__APP__"><img src="../Public/images/logo.png"></a>
				</div>
				<div class="campus">
					<a href="__APP__"><img src="../Public/images/shahe_campus.png"></a>
				</div>
				<div class="search">
					<div class="search-bar pull-right">
						<div class="input-append">
							<input class="span2 search-content" type="text" />
							<button class="btn btn-info btn-search"><span class="icon-search"></span></button>	
						</div> 
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="func">
					<ul class="func-bar pull-right">
						<li><img src="../Public/images/home.png"><a href="http://www.uestc.edu.cn/">学校主页</a></li>
						<li><img src="../Public/images/study.png"><a href="__APP__/Publication/index?type=5">工作研究</a></li>
						<li><img src="../Public/images/admin.png"><a href="#admin-login" data-toggle="modal">管理登入</a></li>
						<li><img src="../Public/images/visit.png"><a href="#">总访问量&nbsp;&nbsp;<span><?php echo $vister;?></span></a></li>
					</ul>
				</div>
				<div id="admin-login" class="modal hide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4>管理登入</h4>
					</div>
					<div class="modal-body">
						<form method="post" name="myform" action="__APP__/Login/checkAdmin">
							<label>用户名：</label>
							<input type="text" name="username">
							<label>密  码：</label>
							<input type="password" name="password" style="display:block;">
							<button class="btn btn-primary">登陆</button>
						</form>
					</div>
				</div>
			</div>
			<div id="nav">
				<ul id="menu">
					<!-- <li><a href="#">校区总览</a>
						<ul>
							<li><a href="__APP__/Publication/detail?id=1">校区概况</a></li>
							<li><a href="__APP__/Publication/detail?id=2">管理机构</a></li>
							<li><a href="#">常驻学院</a></li>
							<li><a href="#">部门黄页</a></li>
							<li><a href="#">魅力沙河</a></li>
						</ul>
					</li>
					<li><a href="__APP__/Publication/index?type=2">规章制度</a></li>
					<li><a href="#">公共服务</a>
						<ul>
							<li><a href="#">特色服务</a></li>
							<li><a href="#">实用信息</a></li>
							<li><a href="#">办事流程</a></li>
							<li><a href="__APP__/Download">资料下载</a></li>	
						</ul>
					</li>
					<li><a href="__APP__/Service/index">办事大厅</a></li> -->
					<?php foreach($menu['parent'] as $key=>$value){ ?>
							<li pid=<?=$value['id']?>> 
								<a href="<?=empty($value['link'])?(empty($value['article'])?"__APP__/Publication/index?type=".$value['board']:"__APP__/Publication/detail?id=".$value['article']):str_replace("[__APP__]",__APP__,$value['link'])?>"><?=$value['name']?></a>
								<ul style="left:0;">
								<?php foreach($menu['son'] as $key=>$value_son){ ?>
									<?php if($value_son['parent_id']==$value['id']){ ?>
										<li><a href="<?=empty($value_son['link'])?(empty($value_son['article'])?"__APP__/Publication/index?type=".$value_son['board']:"__APP__/Publication/detail?id=".$value_son['article']):str_replace("[__APP__]",__APP__,$value_son['link'])?>"><?=$value_son['name']?></a></li>
									<?php } ?>
								<?php } ?>
								</ul>
							</li>
						<?php } ?>
					
					<div class="clearfix"></div>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
<div id="content">
    <div id="leftdiv">
      <div id="conv">
	  <h1>

 <?php echo $typeList[0]['name'];?>
        </h1>
		<div class="clear"></div>
        <ul>

<?php
if (!$nowType) { foreach($current as $key=>$value) { ?>
        <li><a href="__APP__/<?php echo MODULE_NAME;?>/detail?id=<?php echo $value['id'];?>" title="<?php echo $value['title'];?>" class="update_list"><?php echo msubstr($value['title'],0,10);?></a></li>

<?php } } else { foreach($nowType as $key => $value) { ?>
         <li><a href="__APP__/<?php echo MODULE_NAME;?>/index/?type=<?php echo $value['id'];?>"  class="update_list"><?php echo $value['name'];?></a></li>
<?php } } ?>

        </ul>
      </div>

    </div>
    <!--以下是右侧内容-->
    <div id="rightdiv" class="corner">
		<ul id="crumbs">
			<li><a href="__APP__">首页</a></li>
            <?php
 $length = count($typeList); foreach($typeList as $k => $v) {?>
             <?php if ($k+1 != $length) { ?>
                <li><a href="__URL__/index/?type=<?php echo $v['id'];?>"><?php echo $v['name'];?></a></li>
            <?php } else {?>
                <li><?php echo $v['name'];?></li>
            <?php } }?>
</ul>
	  <div class="clear"></div>
        <br />
	  <ul>
      <?php foreach($result['result'] as $key => $value) { ?>
		<li class="paging_news_list_item"><span class="news_sort" style="margin-right: 5px;">[<?php if (!$value['type_name']){echo $typeList[0]['name'];}else{echo $value['type_name'];}?>]</span>&nbsp;&nbsp;&nbsp;<a class="paging_news" title="<?php echo $value['title'];?>" href="__APP__/<?php echo MODULE_NAME;?>/detail?id=<?php echo $value['id'];?>"><?php echo msubstr($value['title'],0,28);?></a><?php if($value['pic']==1){ ?><span class="paging_news_list_pic">[图]</span><?php } ?><span class="paging_news_time"><?php echo date("y-m-d",$value['time']);?></span></li>
	<?php } ?>
    	<!--最多一页拿取20条 否则溢出页面 -->
	  </ul>
      <br />
	  <div id="page_num"><?php echo $result['page'];?></div>
    </div>
  <div class="clear"></div>
</div>
 <div id="footer_pg">
    <p>
    电子科技大学沙河校区管理委员会@版权所有<br/>沙河校区管理委员会办公室地址:成都市建设北路二段四号 沙河校区主楼中332室 邮编:610054<br/>联系电话：028-83201875
    </p>
  </div>
</body>
</html>