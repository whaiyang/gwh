<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en_US" xml:lang="en_US">
<!--
 * Created on 2012-10-9
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
-->
 <head>
     <title>预约登录 </title>
     <link rel="stylesheet" type="text/css" href="../Public/lib/bootstrap-3.2.0/css/bootstrap.min.css" />
     <script type="text/javascript" src="../Public/js/jquery.min.js"></script>
     <style>
         body {
             color: #797979;
             background: #f1f2f7;
             font-family: 'Open Sans', sans-serif;
             padding: 0px !important;
             margin: 0px !important;
             font-size: 13px;
         }
         .form-signin {
             max-width: 330px;
             margin: 100px auto 0;
             background: #fff;
             border-radius: 5px;
             -webkit-border-radius: 5px;
         }
         .form-signin h2.form-signin-heading {
             margin: 0;
             padding: 20px 15px;
             text-align: center;
             background: #41cac0;
             border-radius: 5px 5px 0 0;
             -webkit-border-radius: 5px 5px 0 0;
             color: #fff;
             font-size: 18px;
             text-transform: uppercase;
             font-weight: 300;
             font-family: 'Open Sans', sans-serif;
         }
         .login-wrap {
             padding: 20px;
         }
         .form-signin input[type="text"], .form-signin input[type="password"] {
             margin-bottom: 15px;
             border-radius: 5px;
             -webkit-border-radius: 5px;
             border: 1px solid #eaeaea;
             box-shadow: none;
             font-size: 12px;
         }
         .form-signin .form-control {
             position: relative;
             font-size: 16px;
             height: auto;
             padding: 10px;
             -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
             box-sizing: border-box;
         }
         .form-signin .btn-login {
             background: #f67a6e;
             color: #fff;
             text-transform: uppercase;
             font-weight: 300;
             font-family: 'Open Sans', sans-serif;
             box-shadow: 0 4px #e56b60;
             margin-bottom: 20px;
         }
         .form-signin p {
             text-align: center;
             color: #b6b6b6;
             font-size: 16px;
             font-weight: 300;
         }
     </style>
 </head>
 <body class="login-body">
    <div class="container">
        <form class="form-signin" method="post" action="__APP__/Login/checkOrder">
            <h2 class="form-signin-heading">预约登录</h2>
            <div class="login-wrap">
                <input type="text" name="username" class="form-control" placeholder="用户名" autofocus="">
                <input type="password" name="password" class="form-control" placeholder="密码">
                <input type="text" name="apartname" class="form-control apart-name" placeholder="部门名称"/>
                <p>注:请使用学校信息门户系统账号登陆</p>
                <button class="btn btn-lg btn-login btn-block" type="submit">登录</button>
            </div>
        </form>
    </div>
    <script>
        $(document).ready(function() {
            function validate()
            {
                var value = $(".apart-name").val();
                if (!value) {
                    alert("请输入部门名称");
                    $(".apart-name").focus();
                    return false;
                }
                $(".form-signin").submit();
            }
            $(".btn-login").click(validate);
        })
    </script>
 </body>
</html>