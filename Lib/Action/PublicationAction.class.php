<?php
class PublicationAction extends BaseAction{
    public $tabPublication = null;//文章表
    const  PAGE_SIZE    = 20;
    
    public function _initialize() {
        parent::_initialize();
        $this->tabPublication = D('Publication');
    }
    function index(){ 
        $type = intval($_GET['type']);
        //得到同级别的分类  和当前类别
        $secondType = false;//是否是二级分类
        $typeList = array();//当前类别
        $nowType  = null;//同级别
        if ($type > 0) {
            $t = TypeModel::getTypeById($type);
            if ($t['parent_id'] > 0) { //如果是子类
                $secondType = true;
                $typeList[] = TypeModel::getTypeById($t['parent_id']);
                $nowType    = TypeModel::getChildType($t['parent_id']);
            } else {
                $nowType = TypeModel::getChildType($t['id']);
            }
            $typeList[] = $t;
        }
       
       
       
        $this->assign('typeList', $typeList);
		$this->assign('nowType', $nowType);
        
        if ($secondType) {
            $map['cd_publication.type_2'] = array('eq', $type);
        } else {
            $map['cd_publication.type'] = array('eq', $type);
        }
        $map['cd_publication.private'] = array('eq', 1);
        list($result['page'], $result['result']) = $this->showPage($this->tabPublication, self::PAGE_SIZE, $map,"time DESC" , "LEFT JOIN `cd_type` ON cd_publication.type_2 = cd_type.id", "cd_publication.*, cd_type.name as type_name, cd_type.id as type_id");
        
        $result['title'] = $secondType ? $typeList[1]['name'] : $typeList[0]['name'];
		$this->getMenu();
		
        $this->assign("result",$result);
        $this->display();
    }
    function detail(){
        
        $id   = intval($_GET['id']);
        //得到type list
        $typeList = array();
        if ($id > 0){
            $result = $this->tabPublication->where("`id` = '$id' AND private = 1")->find();
            if ($result) {
                $typeList = TypeModel::getArticleTypes(array($result['type'], $result['type_2']));
                $nowType  = TypeModel::getChildType($typeList[0]['id']);
            } else {
                $this->redirect->redirected("文章不存在！",__APP__,2);
            }
        }
        
        
        if (in_array($id, array(2, 3, 4, 5))) {
            $nowType = null;
            $news=$this->tabPublication->where("`type`='".$result['type']."'")->order("id asc")->limit(9)->select();
            $this->assign("current",$news);
        }
        $this->assign('typeList', $typeList);
        $this->assign('nowType', $nowType);

        $this->tabPublication->query("update cd_publication set `reads`=`reads`+1 where `id`='".$id."'");//
        $this->assign("result",$result);
        $this->display();
    }
    function others(){
        $news=$this->tabPublication->order("time DESC")->limit(9)->select();
        $this->assign("current",$news);


        $id=intval($_GET['id']);
        $result=$this->tabPublication->where("id='".$id."' AND type=0")->find();

        if($result){
            $this->assign('result',$result);
            $this->display('Publication:detail');
        }else{
            $this->redirect->redirected('不存在',__APP__,2);
        }
    }
    function contribute() {
        
        $id=intval($_GET['id']);
        if ($id > 0) {
            self::adminCheck();
        }
        $type=intval($_GET['type']);//判断添加的是Publication（1）还是 新闻（2）
        $result=D("Publication")->where("id='$id'")->find();
        $this->assign("type",$type);
        $this->assign("result",$result);
        $this->display();
    }
    function online_post() {
        $data = array();
        $id = intval($_POST['id']);
        if ($id > 0) {
            self::adminCheck();
        }
        $data['title'] = htmlspecialchars($_POST['title']);
        $data['type']   = intval($_POST['type_1']);
        $data['type_2'] = intval($_POST['type_2']);
        $data['author'] = htmlspecialchars($_POST['author']);
        $data['source'] = htmlspecialchars($_POST['source']);
        if(strpos($_POST['myContent'],"<img")){
            $data['pic'] = 1;
        }
        $data['private'] = 0;
        $data['content'] = htmlspecialchars($_POST['myContent']);
        $data['time'] = time();
        if(empty($_POST['title']) || empty($_POST['author']) || empty($_POST['source']) || empty($_POST['myContent'])) {
            $this->redirect->redirected("请填写完整信息", __APP__."/Publication/contribute", 1);
		}
        if($id>0){
            $result = D('Publication')->where("`id`='".$id."' AND `type`!=0")->save($data);
        } else{
            $result = D('Publication')->add($data);
        }
        if($result) {
            $this->redirect->redirected("投稿成功,等待管理员审核", __APP__."/Publication/contribute",1);
        } else {
            $this->redirect->redirected("投稿失败", __APP__."/Publication/contribute",1);
        }
    }
}