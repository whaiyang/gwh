<?php
class DownloadAction extends BaseAction{

    function _initialize() {
        parent::_initialize();
       
        $typeList = array();
        $typeList[0]['name'] = '资料下载';
        $this->assign('typeList', $typeList);
	}
    function index(){
        $this->assign("title","Download");
        import("ORG.Util.Page");    //count总数；num每页显示数
        $num = D("Download")->count();
        $page=new Page($num,20);
        $result['page']=$page->show();
        $result['result']=D("Download")->limit($page->firstRow.','.$page->listRows)->order('id DESC')->select();
        $this->assign("type",'资料下载');
        $this->assign("result",$result);
        $this->display("Publication:index");
    }
    function detail(){
        $id=intval($_GET['id']);
        $result=D("Download")->where("id='$id'")->find();
        if($result){
            $this->assign("title",$result['title']);

            $this->assign("result",$result);
            $this->display("Publication:detail");
        }
        else{
            $this->redirect->redirected("下载不存在！",__URL__,1);
        }
    }
    function privite(){
        $this->check_privite();
        $result=D("Download")->where("privite=1")->order("id desc")->select();
        $this->assign("type",'Privite');
        $this->assign("result",$result);
        $this->display();
    }
    function download(){
        $id=intval($_GET['id']);
        $result=D("Download")->where("id='$id'")->find();
        $file = "download/".$result["attachment"];
        $filename = $result['attachment'];

        $this->download_($file,$filename);

    }
}