<?php
/**
 * @author myj
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';

class AdminOrderAjax extends AjaxBase {
	public $meeting = null;
	public function __construct() {
		parent::__construct();
		$this->meeting = D("Meeting");
	}
	/**
     * @brief 预约删除
     */
    public function order_delete() {
        $id = intval($_GET['id']);
        $result = $this->meeting->where("id='$id'")->delete();
        if ($result) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
    }
	/**
     * @brief 预约审核
     */
	function check_order() {
		$id = intval($_GET['id']);
		$result=$this->meeting->where("id='$id'")->setField('authority', 1);
		if ($result) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
	}
}
?>