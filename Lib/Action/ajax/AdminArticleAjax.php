<?php
/**
 * @author myj
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';

class AdminArticleAjax extends AjaxBase {
		public $article = null;
		public function __construct() {
			parent::__construct();
			$this->article = D("Publication");
		}
		/**
	     * @brief 文章删除
	     */
	    function delete(){
			$id=intval($_GET['id']);
			$result=$this->article->where("id='$id'")->delete();
			if ($result) {
	            echo json_encode(array('error' => 0, 'message' => ''));
	        } else {
	            echo json_encode(array('error' => 1, 'message' => '删除失败'));
	        }
	        exit();
		}
		/**
	     * @brief 文章审核
	     */
		function check_article() {
			$id=intval($_GET['id']);
			$result=$this->article->where("id='$id'")->setField('private', 1);
			if ($result) {
	            echo json_encode(array('error' => 0, 'message' => ''));
	        } else {
	            echo json_encode(array('error' => 1, 'message' => '删除失败'));
	        }
	        exit();
		}
		/**
	     * @brief 文章置顶
	     */
		function moveUp(){
			$id=intval($_GET['id']);
			if($this->article->where("id='".$id."'")->setField('time',time())){
				echo json_encode(array('error' => 0, 'message' => ''));
			} else {
				echo json_encode(array('error' => 1, 'message' => '删除失败'));
			}
			exit();
		}
		/**
		 * @author zzq
		 * @brief 添加文章类型
		 */
        public function addType() {
            $data = array();
            $data['name']      = htmlspecialchars($_POST['name']);
            $data['parent_id'] = htmlspecialchars($_POST['parent_id']);
            if (D("Type")->add($data)) {
                 echo json_encode(array('error' => 0, 'message' => ''));
	        } else {
	            echo json_encode(array('error' => 1, 'message' => '添加失败'));
	        }
	        exit();
        }
        /**
         * @author zzq
         * @brief 类别删除
         */
        public function delType() {
            $id = intval($_POST['id']);
            if ($id > 0) {
                if (D('Type')->where("`id` = " .$id ." OR `parent_id` = " .$id)->setField('del',1)) {
                     echo json_encode(array('error' => 0, 'message' => ''));
                } else {
                    echo json_encode(array('error' => 1, 'message' => '删除失败'));
                }
               
            } else {
                echo json_encode(array('error' => 1, 'message' => '删除失败'));
            }
            exit();
        }
       /**
         * @author zzq
         * @brief 文章类型修改
         */
        public function editType() {
            $id   = intval($_POST['id']);
            $name = htmlspecialchars($_POST['name']);
            $arr = explode("|", $name);
            $name = $arr[0];
            if(!empty($arr[1]))
                D('Type')->where("`id` = " .$id)->setField('extra',strtolower($arr[1]));

            if (D('Type')->where("`id` = " .$id)->setField('name',$name)) {
                echo json_encode(array('error' => 0, 'message' => ''));
            } else {
                echo json_encode(array('error' => 1, 'message' => '修改失败'));
            }
            exit();
        }
        /**
         * @author zzq
         * @brief 是否在前台展示
         */
        public function showType() {
            $id   = intval($_POST['id']);
            $show = intval($_POST['show']);
            $s    = ($show == 1) ? 0:1;
            if (D('Type')->where("`id` = " .$id)->setField('show',$s)) {
                echo json_encode(array('error' => 0, 'message' => ''));
            } else {
                echo json_encode(array('error' => 1, 'message' => '修改失败'));
            }
            exit();
            
        }
}
?>