<?php
/**
 * @author zzq
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';
require_once MYROOT . '/my/conf/db/ImageConf.php';

class AdminImageAjax extends AjaxBase {
    public $image = null;
    public function __construct() {
        parent::__construct();
        $this->image = D('Image');
        
    }
    /**
     * @brief 图片删除
     */
    public function delImage() {
        $id     = intval($_POST['id']);
        $result = $this->image->where("id='$id'")->delete();
        if ($result) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
    }
    /**
     * @brief 添加类别
     */
    public function addType() {
        $data = array();
        $maxVal        = EmptyAction::getConfSheet()->where("`sheet` = '" . BaseConf::IMAGE . "' AND `field` = '" .ImageConf::$FIELD_NAME['type'] ."'")->order("value DESC")->find();
        $data['name']  = htmlspecialchars($_POST['name']);
        $data['sheet'] = BaseConf::IMAGE;
        $data['field'] = ImageConf::$FIELD_NAME['type'];
        $data['value'] = $maxVal['value'] + 1;

        if (trim($data['name']) == "") {
            echo json_encode(array('error' => 1, 'message' => '填写类别名字'));
        } else {
            if (EmptyAction::getConfSheet()->add($data)) {
                echo json_encode(array('error' => 0, 'message' => ''));
            } else {
                echo json_encode(array('error' => 1, 'message' => '添加失败'));
            }
        }
        exit();
    }
    /**
     * @brief 删除类别
     */
    public function delType() {
        
        $id = intval($_POST['typeID']);
        if (EmptyAction::getConfSheet()->where("`id` = '" .$id. "' AND `sheet` = '" .BaseConf::IMAGE. "' AND `field` = '" .ImageConf::$FIELD_NAME['type']. "'")->delete()) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
    }
    /**
     * @brief 修改分类
     */
    public function editType() {
        $id   = intval($_POST['typeId']);
        $name = htmlspecialchars($_POST['name']);
        if (EmptyAction::getConfSheet()->where("`id` = '" .$id. "' AND `sheet` = '" .BaseConf::IMAGE. "' AND `field` = '" .ImageConf::$FIELD_NAME['type']. "'")->setField('name',$name)) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
    }
    /**
     * @brief 封面设置
     */
    public function coverImage() {
        $id = intval($_POST['id']);
        $image = $this->image->where("`id` = '" .$id. "'")->find();
        if ($image) {
            $type  = $image['type'];
            $cover = $image['thumb'];
            if (EmptyAction::getConfSheet()->where("`sheet` = '" .BaseConf::IMAGE. "' AND `field` = '" .ImageConf::$FIELD_NAME['type']. "' AND `value` = '" .$type. "'")->setField('other',$cover)) {
                echo json_encode(array('error' => 0, 'message' => ''));exit();
            }
        }
        echo json_encode(array('error' => 1, 'message' => '删除失败'));exit();
    }
    /**
     * @brief 置顶
     */
    public function setUpImage() {
        $id = intval($_POST['id']);
        $image = $this->image->where("`id` = '" .$id. "'")->setField('time1',time());
        if ($image) {
            echo json_encode(array('error' => 0, 'message' => ''));exit();
        }
        echo json_encode(array('error' => 1, 'message' => '操作失败'));exit();
    }
}