<?php
/**
 * @author myj
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';

class AdminServiceAjax extends AjaxBase {
	public $service = null;
	public function __construct() {
		parent::__construct();
		$this->service = D("Service");
	}
	/**
     * @brief 删除
     */
	function serv_delete() {
		$id=intval($_GET['id']);
		$result=$this->service->where("id='$id'")->delete();
		if ($result) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
	}
	/**
     * @brief 置顶
     */
	function serv_moveUp() {
		$id=intval($_GET['id']);
		if ($this->service->where("id='".$id."'")->setField('time',time())) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
	}
	/**
	 * @author zzq
	 * @brief 添加类型
	 */
    public function addType() {
        $data = array();
        $data['name']      = htmlspecialchars($_POST['name']);
        $data['parent_id'] = htmlspecialchars($_POST['parent_id']);
        if (D("Type")->add($data)) {
             echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '添加失败'));
        }
        exit();
    }
    /**
     * @author zzq
     * @brief 类别删除
     */
    public function delType() {
        $id = intval($_POST['id']);
        if ($id > 0) {
            if (D('Type')->where("`id` = " .$id ." OR `parent_id` = " .$id)->setField('del',1)) {
                 echo json_encode(array('error' => 0, 'message' => ''));
            } else {
                echo json_encode(array('error' => 1, 'message' => '删除失败'));
            }
           
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
    }
   /**
     * @author zzq
     * @brief 类型修改
     */
    public function editType() {
        $id   = intval($_POST['id']);
        $name = htmlspecialchars($_POST['name']);
        if (D('Type')->where("`id` = " .$id)->setField('name',$name)) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '修改失败'));
        }
        exit();
    }
    /**
     * @author zzq
     * @brief 是否在前台展示
     */
    public function showType() {
        $id   = intval($_POST['id']);
        $show = intval($_POST['show']);
        $s    = ($show == 1) ? 0:1;
        if (D('Type')->where("`id` = " .$id)->setField('show',$s)) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '修改失败'));
        }
        exit();
        
    }
}
?>