<?php
/**
 * @author myj
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';

class AdminApartAjax extends AjaxBase {
	public $apart = null;
	public function __construct() {
		parent::__construct();
		$this->apart = D("Apart");
	}
	/**
     * @brief 删除部门
     */
	function delete_apart() {
		$id=intval($_GET['id']);
		$result=$this->apart->where("id='$id'")->delete();
		if ($result) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
	}
}
?>