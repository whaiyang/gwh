<?php
/**
 * @author myj
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';

class IndexAjax extends AjaxBase {
    public $meeting = null;
    public function __construct() {
        parent::__construct();
        $this->meeting = D("Meeting");
    }
	/**
     * @brief 取消预约
     */
    public function cancel() {
        if($_SESSION['order_login'] == true) {
			$id=intval($_GET['id']);
			$result=D("Meeting")->where("id='$id'")->delete();
			if($result){
				echo json_encode(array('error' => 0, 'message' => ''));exit();
			}
			else{
				echo json_encode(array('error' => 1, 'message' => '删除失败'));exit();
			}
		} else {
			$this->redirect->redirected("非法操作", __APP__."/Index/meeting", 2);
		}
}
}
?>