<?php
/**
 * @author myj
 */
require_once MYROOT . '/Lib/Action/ajax/AjaxBase.php';

class AdminDownloadAjax extends AjaxBase {
	public $download = null;
	public function __construct() {
		parent::__construct();
		$this->download = D("Download");
	}
	/**
     * @brief 删除下载
     */
	function delete_download(){
		$id=intval($_GET['id']);
		$result=$this->download->where("id='$id'")->delete();
		if ($result) {
            echo json_encode(array('error' => 0, 'message' => ''));
        } else {
            echo json_encode(array('error' => 1, 'message' => '删除失败'));
        }
        exit();
	}

}
?>