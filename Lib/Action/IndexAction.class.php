<?php
// 本类由系统自动生成，仅供测试用途
require_once MYROOT . '/my/conf/db/ImageConf.php';
require_once MYROOT . '/why/constant.php';
class IndexAction extends BaseAction {
    public $image = null;
    
    public function _initialize() {
        parent::_initialize();
        $this->image = D('Image');
      
	}
		public function index(){
				//$result=new array();
				$Publicaton=D("Publication");
				$leftData = M("type")->where("parent_id=1 AND extra='left'")->select();
				$rightData = M("type")->where("parent_id=1 AND extra='right'")->select();
				$left = "";
				$right = "";
				foreach ($leftData as $key=>$value) {
					$left .= " type_2=".$value['id'];
					if($key<count($leftData)-1)
						$left .= " OR ";
				}
				foreach ($rightData as $key=>$value) {
					$right .= " type_2=".$value['id'];
					if($key<count($rightData)-1)
						$right .= " OR ";
				}
				$result['school']['importantNews']=$Publicaton->where("type=1 AND private=1 AND ((($left) AND (extra<>'right' OR extra is null)) OR (extra='left' AND ($right)))")->order("time desc")->limit(5)->select();
				$result['school']['apart']=$Publicaton->where("type=1  AND private=1 AND ((($right) AND (extra<>'left' OR extra is null)) OR (extra='right' AND ($left)))")->order("time desc")->limit(5)->select();
				//echo $Publicaton->getLastSql();
				$result['school']['activities']=$Publicaton->where("type=4 AND type_2=81 AND private=1")->order("time desc")->limit(5)->select();
				$result['rules']=$Publicaton->where("type=2 AND private=1")->order("time desc")->limit(8)->select();
				$result['service']=$Publicaton->where("type=3 AND private=1")->order("time desc")->limit(8)->select();
				$result['notice']=$Publicaton->where("type=4 AND (type_2=31 OR type_2=82) AND private=1")->order("time desc")->limit(5)->select();
				$result['jiangzuo'] = $Publicaton->where("type=4 AND type_2=30 AND private=1")->order("time desc")->limit(5)->select();
				$result['recommendation']=D('Msg')->where('`read`=1')->order("id desc")->limit(6)->select();



				$result['pic_up']   = $this->image->where("type=1")->order("time1 desc, id desc")->limit(6)->select();
				$result['pic_down'] = $this->image->where("type > 1")->order("time1 desc, id desc")->limit(6)->select();
				/**
					author:WHY
					2013/9/25
				*/
				/*$menu = M('indexmenu');
				$pmenudata = $menu->where('parent_id = 0')->order("zindex desc")->select();
				$smenudata = $menu->where('parent_id <> 0')->order("zindex desc")->select();
				$menuData['parent'] = $pmenudata;
				$menuData['son'] = $smenudata;
				$result['menu'] = $menuData;*/
				
				$image = M("dynamicimage");
				$resultPic = $image->where("`select` = 1")->order("time desc")->select();
				foreach($resultPic as $key=>$value)
				{
					$resultPic[$key]['src'] = PIC_PATH.$resultPic[$key]['savename'];
				}
				$result['slidePic'] = $resultPic;
				$result['month'] = intval(date('m'));
				$result['date'] = date('d');
				$result['hour'] = date('H');
				$result['minute'] = date('i');
				$result['week'] = $this->weekMap(intval(date('w')));
				$this->assign("result",$result);
				$this->display();
		}
	
	/**
		By WHY
		2013/9/23
	*/	
	private function weekMap($week)
	{
		switch($week)
		{
			case 0:return '日';
			case 1:return '一';
			case 2:return '二';
			case 3:return '三';
			case 4:return '四';
			case 5:return '五';
			case 6:return '六';
		}
	}
    /**
     * @author zzq
     */
    public function image() {
        $newType   = array();
        $imageType = $this->confSheet->where("`sheet` = ".BaseConf::IMAGE." AND `field` = '".ImageConf::$FIELD_NAME['type']."' AND `value` > 1")->select();
        $type      = $this->image->field("count(*) as counts,type")->group("type")->select();
        
        foreach ($type as $key => $value) {
            $newType[$value['type']] = $value['counts'];
        }
        foreach ($imageType as $k => $val) {
            $imageType[$k]['count'] = intval($newType[$val['value']]);
        }
        $this->assign("result", $imageType);
        $this->display();
    }
    /**
     * @author zzq
     */
    public function imageList(){
        $type = intval($_GET['type']);
        if ($type <= 1) {
            $result['result'] = $this->image->where("`".ImageConf::$FIELD_NAME['type']."` > 1")->limit(32)->order('id DESC')->select();
        } else {
            $imageType = $this->confSheet->where("`sheet` = ".BaseConf::IMAGE." AND `field` = '".ImageConf::$FIELD_NAME['type']."' AND `value` = '" .$type. "'")->find();
            $result['typeName'] = $imageType['name']; 
            import("ORG.Util.Page");    //count总数；num每页显示数
            $num  = $this->image->where("`" . ImageConf::$FIELD_NAME['type'] . "`='" .$type. "'")->count();
            $page = new Page($num,20);
            $result['page']   = $page->show();
            $result['result'] = $this->image->where("`" . ImageConf::$FIELD_NAME['type'] . "`='" .$type. "'")->limit($page->firstRow.','.$page->listRows)->order('id DESC')->select();
        }
        $result['type'] = $type;
        $this->assign('title', "魅力沙河");
        $this->assign("result", $result);
        $this->display();

    }
   
		function detail(){
				$id=intval($_GET['id']);
				$this->display();

		}
	//会议预约页面
	function meeting() {
		if(!empty($_GET)) {
			$year = htmlspecialchars($_GET['year']);
			$month = htmlspecialchars($_GET['month']);
		}
		$year = intval($year);
		$month = intval($month); 
		if(empty($year)) {
			$year = date('Y');
		}
		if(empty($month)) {
			$month = date('m');
		}
		$cal = new CalendarAction();
		$str = $cal->buildCalendar($year, $month);
		
		$this->assign("str", $str);
		$this->display();
	}
	//预约信息提交
	function order_post() {
		$start_time = strtotime("today") + 8.4*3600;
		$end_time = strtotime("today") + 17.5*3600;

		//7月17日到8月28日期间暂时关闭会议预约
		$webdata = D("Webdata");
		$meeting_close = $webdata->where(array('key'=>'meeting_close'))->find();
		$meeting_close_start = $webdata->where(array('key'=>'meeting_close_start'))->find();
		$meeting_close_end = $webdata->where(array('key'=>'meeting_close_end'))->find();
		$meeting_close = $meeting_close['value'];
		$meeting_close_start = $meeting_close_start['value'];
		$meeting_close_end = $meeting_close_end['value'];
		if ($meeting_close&&strtotime($_POST['order_start']) >= strtotime($meeting_close_start) && strtotime($_POST['order_start']) <= strtotime($meeting_close_end)) {
			$this->redirect->redirected("网上会务预约在".$meeting_close_start."到".$meeting_close_end."期间，暂停预约服务。如需预约会议室，请致电83201875电话预约", __APP__."/Index/meeting", 4);
			return;
		}

		/*$closeStartTime = mktime(0, 0, 0, 1, 22, 2014);
		$closeEndTime = mktime(23, 59, 59, 2, 19, 2014);

		if (strtotime($_POST['order_start']) >= $closeStartTime && strtotime($_POST['order_start']) <= $closeEndTime) {
			$this->redirect->redirected("网上会务预约在寒假（1月22日-2月19日）期间，暂停预约服务。如需预约会议室，请致电83201875电话预约", __APP__."/Index/meeting", 4);
			return;
		}*/
		//dump($_POST);exit;

		if (strtotime(htmlspecialchars($_POST['order_start'])) - strtotime("today") < 24*3600) {
			$this->redirect->redirected("您不能预约当日的会议室,如需预约请致电83201875", __APP__."/Index/meeting", 4);
			exit();
		}

		if(date("w", strtotime("today")) == 0 || date("w", strtotime("today")) == 6) {
			HttpNamespace::redirected("请在周一至周五上班时间预约",__URL__."/meeting",3,"#EEE");
			exit();
		}

		if(!empty($_SESSION['order_login'])) {
			if (time() >= intval($start_time) && time() <= intval($end_time)) {
				$orderdate = strtotime($_POST['order_start']);
				$nowaday = strtotime("today");
				$days = round(($orderdate - $nowaday)/3600/24);
				//dump($days);exit;

				if($days < 0) {
					$this->redirect->redirected("非法预约", __APP__."/Index/meeting", 2);
					exit();
				}

				if($days > 30) {
					$this->redirect->redirected("您的预约超出范围,只能预约近一个月的会议!", __APP__."/Index/meeting", 3);
					exit();
				}else {
					$data = array();
					$data['apart_id'] = $_SESSION['uid'];
					$data['apart_name'] = $_SESSION['apart_name'];
					$data['contacts'] = $_SESSION['user_name'];
					$data['order_room'] = intval(htmlspecialchars($_POST['order_room']));
					$data['video_sel'] = intval($_POST['video_sel']);
					if(intval($_POST['order_time']) > 0 && intval($_POST['order_time']) < 4) {
						$data['order_sel'] = 1;
					}
					if(intval($_POST['order_time']) > 3) {
						$data['order_sel'] = 2;
					}
					$data['order_time'] = intval(htmlspecialchars($_POST['order_time']));
					$data['order_start'] = htmlspecialchars($_POST['order_start']);
					$data['phone_num'] = intval(htmlspecialchars($_POST['phone_num']));
					$data['descript'] = intval(htmlspecialchars($_POST['descript']));
					//dump($data);exit;

					$order_start = $data['order_start'];
					$order_room = $data['order_room'];
					$order_time = $data['order_time'];
					$check = D("Meeting")->where("order_start='$order_start' AND order_room='$order_room' AND order_time='$order_time' AND authority=1")->find();

					if($check) {
						$this->redirect->redirected("对不起,预约时间冲突!", __APP__."/Index/meeting", 2);
					}else {
						$result = D("Meeting")->add($data);
						if($result) {
							$this->redirect->redirected("提交成功,请等待管理员审核", __APP__."/Index/meeting", 2);
						}else {
							$this->redirect->redirected("预约失败", __APP__."/Index/meeting", 2);
						}
					}
				}
			}else {
				$this->redirect->redirected("请在工作时间8:30~17:30预约", __APP__."/Index/meeting", 2);
			}
		} else {
			$this->redirect->redirected("请先登录再预约", __APP__."/Login", 2);
		}
	}

	function order_cancel() {
		$uid = $_SESSION['uid'];
		$result = D("Meeting")->where("apart_id='$uid'")->select();
		//dump($result);exit;

		$this->assign('result',$result);
		$this->display();
	}

	function cancel() {
		if($_SESSION['order_login']) {
			$id=intval($_GET['id']);
					$result=D("Meeting")->where("id='$id'")->delete();
					if($result){
							echo 1;exit;
					}
					else{
							echo 0;exit;
					}
			} else {
				$this->redirect->redirected("非法操作", __APP__."/Index/meeting", 2);
			}
	}
}
