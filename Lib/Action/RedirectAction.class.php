<?php
class RedirectAction extends BaseAction
{
    function _initialize()
	{
       $this->app=__ROOT__;
    }
    
    function index(){
        $action=$_GET['action'];
        if ($action == "login")
        {
        	$content = "恭喜您成功登陆!";
        	$url = __APP__;
        }
        
        if ($action == "logwrong")
        {
        	$content = "用户名密码错误!";
        	$url = __APP__."/user/login";
        }
        
        if ($action == "set")
        {
        	$content = "修改成功!";
        	$url = __APP__."/user/setting";
        }
        else if($action==""){
            $content = "请您登陆!";
            $url = __APP__;
        }
        $this->redirected($content,$url);
    }
    function redirected($content, $goto="",$stop=1,$color="#E1EBEF")
    {
        if(empty($color)){
            $color="#E1EBEF";
        }
    	if ($goto){
    		$refresh	= "<meta http-equiv=\"refresh\" content=\"$stop;URL=$goto\">";
    		$action		= "<a href=\"$goto\">如果您的浏览器没有自动跳转,请点击这里</a>";
    	}else if($goto==""){
            if($content!=""){
                $refresh	= '';
    		    $action		= '<a href="#" onclick="javascript:history.go(-1)"><< 返 回 继 续 操 作</a>';
            }
            if($content==""){
                $goto=__APP__;
                $refresh	= "<meta http-equiv=\"refresh\" content=\"$stop;URL=$goto\">";
    		    $action		= "<a href=\"$goto\">如果您的浏览器没有自动跳转,请点击这里</a>";
            }		
    	}
        $html = <<<EOT
    	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml">
    	<head>
    	<link href="$this->app/Tpl/Public/css/register.css" type="text/css" rel="stylesheet" />
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	$refresh
    	<title>Prompting Message !</title>
    	</head>
    	<body style="background-color: $color;">
    	<div id="wrap">
    		<div id="succeed">
    			<p id="registerOK" style="text-align:center;">$content</p>
    			<p id="welcJoin"><strong>沙河校区管理委员会</strong></p>
    			<p id="jump">$action</p>
    		</div>
    	</div><!--wrap-->
    	</body>
    	</html>
EOT;

	exit($html);
 }//结束个人添加
}
?>