<?php
	$_REQUEST_URI = explode('/',$_SERVER["REQUEST_URI"]);
	$APPNAME = $_REQUEST_URI[1];
	
	define('WHY_APPNAME',$APPNAME);
	define('WHY_DOMAIN',$_SERVER['HTTP_HOST']);
	define('PIC_PATH','./Tpl/Public/uploads/');
	define('PIC_SRC','http://'.$_SERVER['HTTP_HOST'].'/'.WHY_APPNAME.'/Tpl/Public/uploads/');
	
	class AdminImageWhyAction extends Action
	{
		/**
			author:WHY
			2013/9/24
		*/
		public function dynamicImage()
		{
			$images = M("dynamicimage");
			$imageData = $images->where()->order('time desc')->select();
			foreach($imageData as $key=>$value)
			{
				$path = str_replace('.', '', $value['src']);
				$name = "thumb_".$value['savename'];
				$imageData[$key]['src'] = 'http://'.$_SERVER['HTTP_HOST'].'/'.WHY_APPNAME.$path.$name;
			}
			$this->assign('imageData',$imageData);
			$this->display();
		}
	
		public function fileIntitle()
		{
			return uniqid(time());
		}
		/**
			author:WHY
			2013/9/24
		*/
		public function dynamicImageUpload()
		{
			import('ORG.Net.UploadFile');
			foreach($_POST as $key=>$value)
			{
				$_POST[$key] = htmlspecialchars($value);
			}
			$upload = new UploadFile();// 实例化上传类
			$upload->maxSize  = 20*1024*1024;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  PIC_PATH;// 设置附件上传目录
			$upload->uploadReplace = true;//同名文件覆盖
			$upload->thumb = true;//缩略图配置
			$upload->thumbMaxWidth = '70';
			$upload->thumbMaxHeight = '70';
			$upload->saveRule = time;
			if(!$upload->upload())
			{// 上传错误提示错误信息
				//$this->error($upload->getErrorMsg());
				$this->ajaxReturn("Upload failed ".$upload->getErrorMsg(),0,0);
			}
			else
			{// 上传成功 获取上传文件信息
				$info =  $upload->getUploadFileInfo();
				$image = M("dynamicimage");
				$time = time();
				$pid = $image->add(array('name'=>$_POST['Filename'],'savename'=>$info[0]['savename'],'src'=>$info[0]['savepath'],'time'=>$time));
				$pthumbsrc = PIC_SRC."thumb_".$info[0]['savename'];
				$pname = $_POST['Filename'];
				$ptitle = "";
				$plink = "";
				$pselect = "<Button url=".__APP__ ."/AdminImageWhy/selectPic/id/".$pid." class=selectPic>选择</Button>";
				$html = "<tr picid=".$pid."><td><img width=70 height=70 src='".$pthumbsrc.
				"'/></td><td>".$pname."</td><td class='ptl ptl_t'>".$ptitle."</td><td class='ptl ptl_l'>".$plink."</td><td>".$pselect.
				"</td><td>".date('Y年/m月/d日 H时',intval($time))."</td><td><img id='".$pid.
				"' style='display:none;' width=18 height=18 src='../Public/img/loading.gif'/><Button picid=".$pid.
				" url='".__APP__."/AdminImageWhy/deletePic/id/".$pid."' class='deletePic' style='font-weight:bold'>删除</button></td></tr>";
				$this->ajaxReturn($html,1,1);
			}
		}
		/**
			author:WHY
			2013/9/24
		*/
		public function deletePic()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			$image = M("dynamicimage");
			$result = $image->where("id = ".$_GET['id'])->select();
			$fname1 = PIC_PATH.$result[0]['savename'];
			$fname2 = PIC_PATH."thumb_".$result[0]['savename'];
			if(unlink($fname1)&&unlink($fname2))
			{
				$result = $image->where("id = ".$_GET['id'])->delete();
				if($result)
					echo $_GET['id'];
				else
					echo 0;
			}
			else
				echo 0;
		}
		
		/**
			author:WHY
			2013/9/25
		*/
		public function selectPic()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			
			$image = M("dynamicimage");
			$data = $image->where('id = '.$_GET['id'])->limit(1)->select();
			$d = 1-intval($data[0]['select']);
			$result = $image->where('id = '.$_GET['id'])->save(array('select'=>$d));
			if(!empty($result))
				echo 1;
			else
				echo 0;
		}
		
		/**
			author:WHY
			2013/9/25
		*/
		public function setTitleLink()
		{
			foreach($_GET as $key=>$value)
			{
				//echo $key."————".$value."   ";
				//$_GET[$key] = htmlspecialchars($value);
			}
			$image = M("dynamicimage");
			$result = $image->where('id = '.$_GET['id'])->save(array('title'=>$_GET['title'],'link'=>$_GET['link']));
			if(!empty($result))
				echo 1;
			else
				echo 0;
		}
	}