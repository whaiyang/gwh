<?php
/**
 * @author zzq
 */
require_once MYROOT . '/my/conf/db/ImageConf.php';

class ImageAction extends BaseAction{
    public function save($filepath,$file_name,$time) {
        $filename = $filepath."/".$time.'/'.$file_name;
        
        $image =  getimagesize($filename);
       
        
        if(false !== $image) {
            
            //��ͼ���ļ��������ͼ
            $thumbWidth		=	200;
            $thumbHeight		=	1000;
            
            
          
            // ���ͼ������ͼ
            
            
           
            $thumbname	=	$filepath."/".$time.'/'.'/thumb-'.$this->extend($file_name).'.'.$this->extendss($file_name);
            $this->thumb($filename,$thumbname,'',$thumbWidth,$thumbHeight,true);
            
            
                //unlink($filename);
        }
        return true;
    }
    public function thumb($image, $thumbname, $type='', $maxWidth=200, $maxHeight=50, $interlace=true) {
        // ��ȡԭͼ��Ϣ
        
        $info = $this->getImageInfo($image);
        
        if ($info !== false) {
            $srcWidth = $info['width'];
            $srcHeight = $info['height'];
            $type = empty($type) ? $info['type'] : $type;
            $type = strtolower($type);
            $interlace = $interlace ? 1 : 0;
            unset($info);
            $scale = min($maxWidth / $srcWidth, $maxHeight / $srcHeight); // �������ű���
            if ($scale >= 1) {
                // ����ԭͼ��С��������
                $width = $srcWidth;
                $height = $srcHeight;
            } else {
                // ����ͼ�ߴ�
                $width = (int) ($srcWidth * $scale);
                $height = (int) ($srcHeight * $scale);
            }

            // ����ԭͼ
            $createFun = 'ImageCreateFrom' . ($type == 'jpg' ? 'jpeg' : $type);
            $srcImg = $createFun($image);

            //��������ͼ
            if ($type != 'gif' && function_exists('imagecreatetruecolor'))
                $thumbImg = imagecreatetruecolor($width, $height);
            else
                $thumbImg = imagecreate($width, $height);

            // ����ͼƬ
            if (function_exists("ImageCopyResampled"))
                imagecopyresampled($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
            else
                imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $width, $height, $srcWidth, $srcHeight);
            if ('gif' == $type || 'png' == $type) {
                //imagealphablending($thumbImg, false);//ȡ��Ĭ�ϵĻ�ɫģʽ
                //imagesavealpha($thumbImg,true);//�趨��������� alpha ͨ����Ϣ
                $background_color = imagecolorallocate($thumbImg, 0, 255, 0);  //  ָ��һ����ɫ
                imagecolortransparent($thumbImg, $background_color);  //  ����Ϊ͸��ɫ����ע�͵������������ɫ��ͼ
            }

            // ��jpegͼ�����ø���ɨ��
            if ('jpg' == $type || 'jpeg' == $type)
                imageinterlace($thumbImg, $interlace);

            // ���ͼƬ
            $imageFun = 'image' . ($type == 'jpg' ? 'jpeg' : $type);
            $imageFun($thumbImg, $thumbname);
            imagedestroy($thumbImg);
            imagedestroy($srcImg);
            return $thumbname;
        }
        return false;
    }
     public function getImageInfo($img) {
        $imageInfo = getimagesize($img);
        if ($imageInfo !== false) {
            $imageType = strtolower(substr(image_type_to_extension($imageInfo[2]), 1));
            $imageSize = filesize($img);
            $info = array(
                "width" => $imageInfo[0],
                "height" => $imageInfo[1],
                "type" => $imageType,
                "size" => $imageSize,
                "mime" => $imageInfo['mime']
            );
            return $info;
        } else {
            return false;
        }
    }
    function  extend($file_name)   
    {   
        $extend=explode(".",$file_name);   
        
        return $extend[0];   
    } 
    function  extendss($file_name)   
    {   
        $extend=explode(".",$file_name);   
        $va=count($extend)-1;   
        return $extend[$va];   
    }  
}
?>