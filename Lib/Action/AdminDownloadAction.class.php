<?php
/**
 * @author myj
 *
 */
//require_once MYROOT . '/my/conf/db/DownloadConf.php';

class AdminDownloadAction extends AdminAction {
	public function _initialize() {
        parent::_initialize();
    }

		function download(){
			import("ORG.Util.Page");    //count总数；num每页显示数

			$num = D("Download")->count();
			$page=new Page($num,15);
			$result['page']=$page->show();
			$result['result']=D("Download")->limit($page->firstRow.','.$page->listRows)->order('id DESC')->select();

			$this->assign('result',$result['result']);
			$this->assign('page',$result['page']);
			$this->display();
		}

		function add_download(){

			$id=intval($_GET['id']);
			$result=D("Download")->where("id='$id'")->find();

			$this->assign("result",$result);
			$this->display();
		}

		function ue_download() {
			$data=array();
			$id=intval($_POST['id']);
			$data['title']=trim(htmlspecialchars($_POST['title']));
			$data['content'] = htmlspecialchars(trim($_POST['myContent']));
			$data['time'] = time();

				if($id>0){
						$result = D('Download')->where("`id`='".$id."'")->save($data);
				}else{
						$result = D('Download')->add($data);
				}
			if($result) {
				$this->redirect->redirected("添加成功", __APP__."/AdminDownload/add_download",1);
			}else {
				$this->redirect->redirected("添加失败", __APP__."/AdminDownload/add_download",1);
			}
		}
}
?>