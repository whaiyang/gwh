<?php
/**
 * @author myj
 *
 */
require_once MYROOT . '/my/conf/db/ApartConf.php';

class AdminApartAction extends AdminAction {
	public function _initialize() {
        parent::_initialize();
    }

	function apart() {
		$Total_num = D("Apart")->count();
		$result = D("Apart")->showpage($Total_num, 10);

		$this->assign('result',$result[0]);
		$this->assign('page',$result[1]);
		$this->display();
	}

	function add_apart() {
		$id = intval($_GET['id']);
		$result = D("Apart")->where("id='$id'")->find();

		$this->assign('result',$result);
		$this->display();
	}

	function apart_edit() {
		$data = array();
		$id = $_POST['id'];
		$data['apart_name'] = $_POST['apart_name'];
		$data['apart_user'] = $_POST['apart_user'];
		$data['apart_passwd'] = $_POST['apart_passwd'];

		if($id > 0) {
				$result = D("Apart")->where("id='$id'")->save($data);
				if($result) {
						$this->redirect->redirected("修改成功", __URL__."/apart", 1);
				} else {
						$this->redirect->redirected("修改失败", __URL__."/apart", 2);
				}
		} else {
				$result = D("Apart")->add($data);
				if($result) {
						$this->redirect->redirected("添加成功", __URL__."/apart", 1);
				} else {
						$this->redirect->redirected("添加失败", __URL__."/apart", 2);
				}
		}
	}
}
?>