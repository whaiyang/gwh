<?php
	require_once MYROOT . '/why/constant.php';
	
	/**
		author:WHY
		2013/9/29
	*/
	
	class AdminIndexMenuWhyAction extends Action
	{
		public function index()
		{
			$menu = M('indexmenu');
			$pmenudata = $menu->where('parent_id = 0')->order("zindex desc")->select();
			$smenudata = $menu->where('parent_id <> 0')->order("zindex desc")->select();
			$menuData['parent'] = $pmenudata;
			$menuData['son'] = $smenudata;
			$this->assign('menuData',$menuData);
			$this->display();
		}
		
		public function addPMenu()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			$menu = M('indexmenu');
			$PK = $menu->add(array('name'=>$_GET['menuTitle'],'parent_id'=>0,'link'=>$_GET['menuLink'],'board'=>$_GET['menuBoard'],'article'=>$_GET['menuArticle']));
			if(!empty($PK))
			{
				/*$link = empty($_GET['link'])?__APP__."/Publication/detail?id=".$_GET['menuBoard']:$_GET['menuLink'];
				$html = '<li pid='.$PK.'><a href="'.$link.'">'.$_GET['menuTitle']."</a>";
				$html .= '<ul>';
				$html .= '<li><a class=addmenuclass id="addsmenu">点击添加+</a></li>'; 
				$html .= '</ul>';
				$html .= '</li>';*/
				$html = '';
				$this->ajaxReturn($html,1,1);
			}
			else
				$this->ajaxReturn("",0,0);
		}
		
		public function addSMenu()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			
			if(empty($_GET['pid']))
				$this->ajaxReturn("",0,0);
			$menu = M('indexmenu');
			$PK = $menu->add(array('parent_id'=>$_GET['pid'],'name'=>$_GET['menuTitle'],'link'=>$_GET['menuLink'],'board'=>$_GET['menuBoard'],'article'=>$_GET['menuArticle']));
			if(!empty($PK))
			{
				$html = "";
				$this->ajaxReturn($html,1,1);
			}
			else
				$this->ajaxReturn("",0,0);
		}
		
		public function UpdateMenu()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			
			if(empty($_GET['pid']))
				$this->ajaxReturn("",0,0);
			$menu = M('indexmenu');
			$PK = $menu->save(array('id'=>$_GET['pid'],'name'=>$_GET['menuTitle'],'link'=>$_GET['menuLink'],'board'=>$_GET['menuBoard'],'article'=>$_GET['menuArticle']));
			if(!empty($PK))
			{
				$html = "";
				$this->ajaxReturn($html,1,1);
			}
			else
				$this->ajaxReturn("",0,0);
		}
		
		public function deleteMenu()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			
			if(empty($_GET['pid']))
				$this->ajaxReturn("",0,0);
			$menu = M('indexmenu');
			$result1 = $menu->where('id = '.$_GET['pid'])->delete();
			$result2 = $menu->where('parent_id = '.$_GET['pid'])->delete();
			if($result1)
			{
				$html = "";
				$this->ajaxReturn($html,1,1);
			}
			else
				$this->ajaxReturn("",0,0);
		}
		
		public function changeZindex()
		{
			foreach($_GET as $key=>$value)
			{
				$_GET[$key] = htmlspecialchars($value);
			}
			
			if(empty($_GET['id']))
				$this->ajaxReturn("",0,0);
			$menu = M('indexmenu');
			$PK = $menu->save(array('id'=>$_GET['id'],'zindex'=>$_GET['zindex']));
			if($PK)
			{
				$html = "";
				$this->ajaxReturn($html,1,1);
			}
			else
				$this->ajaxReturn("",0,0);
		}
	}
