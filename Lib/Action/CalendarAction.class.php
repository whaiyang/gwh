<?php
class CalendarAction extends BaseAction
{
	//获取会议室预定信息
	function _loadEventData($year, $month, $day = NULL) {
		/*
		 * 找出这个月第一天和最后一天
		 */
		$start_date = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
		$end_date = date('Y-m-d', mktime(23, 59, 59, $month+1, 0, $year));

		if(!empty($day)) {
			$order_room_date = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
			$result = D("Meeting")->where("order_start='$order_room_date' AND authority=1")->select();
		} else {
			$map['order_start'] = array('between', array($start_date, $end_date));
			$map['authority'] = array('eq', 1);
			$result = D("Meeting")->where($map)->select();
		}

		return $result;
	}

	//载入该月全部预定信息到一个数组
	function _createEventObj($year, $month, $day = NULL) {
		/*
		 * 载入预约数组
		 */
		if(empty($day)) {
			$arr = $this->_loadEventData($year, $month);

			/*
			 * 按照预约活动发生在该月第几天将活动数据重新组织到一个新数组当中
			 */
			$events = array();
			foreach($arr as $event)
			{
				$day = date('j', strtotime($event['order_start']));

				try
				{
					$events[$day][] = new EventAction($event);
					//dump($events);exit;
				}
				catch(Exception $e)
				{
					die($e->getMessage());
				}
			}
		} else {
			$arr = $this->_loadEventData($year, $month, $day);
			//dump($arr);

			$events = array();
			foreach($arr as $event)
			{
				$num = $event['order_room'];
				$sel = $event['order_sel'];

				try
				{
					$events[$num][$sel] = new EventAction($event);
					//dump($events);exit;
				}
				catch(Exception $e)
				{
					die($e->getMessage());
				}
			}
		}
		//dump($events);exit;
		return $events;
	}

	//生成表格日历
	function buildCalendar($year, $month) {
		header("content-type:text/html;charset=utf-8");
		$start_weekday = date('w', mktime(0, 0, 0, $month, 1, $year)); //算出这个月第一天是星期几
		$days = date('t', mktime(0, 0, 0, $month, 1, $year));          //算出这个月有多少天
		$prev_month = $month - 1;
		$prev_days = date('t', mktime(0, 0, 0, $prev_month, 1, $year));
		$week = array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
		$i = 0;
		$j = 0;
		$k = 1;
		$str = "";
		$str.= "<div id='meeting_head'>";
		if(empty($_SESSION['order_login'])) {
			$str.= "<span class='user condition'>您好，您可以先登入，以便进行预约及取消预约</span><br>";
			$str.="<span class='user condition'>如需预约接待室(主楼中320、347)，请致电028-83201875电话预约</span><br>";
			$str.= "<span class='user condition big' >网上预约时间:周一至周五8:30~17:30</span><br>";
			$str.= "<span class='user condition big'>目前，主楼中3楼会议室暂面向机关各职能部门开放</span>";
			$str.= "<a href='__APP__/Login' class='login'>登入</a>";
		} else {
			$str.= "<span class='user condition'><a style='color:red;' href='__APP__/Index/order_cancel'>您好 " . $_SESSION['user_name'] . ",取消预约请点此处>>></a></span>";
			$str.= "<span class='user condition'><a style='color:red;' href='__APP__/Login/order_out'>退出登录</a></span><br>";
			$str.= "<span class='user condition'>网上预约时间:周一至周五8:30~17:30</span><br>";
			$str.= "<span class='user condition'>目前，主楼中3楼会议室暂面向机关各职能部门开放</span>";
		}
		
		
		
		
		/****************************************************/
		/***************************************************/
		$webdata = D("Webdata");
		$meeting_close = $webdata->where(array('key'=>'meeting_close'))->find();
		$meeting_close_start = $webdata->where(array('key'=>'meeting_close_start'))->find();
		$meeting_close_end = $webdata->where(array('key'=>'meeting_close_end'))->find();
		$meeting_close = $meeting_close['value'];
		$meeting_close_start = $meeting_close_start['value'];
		$meeting_close_end = $meeting_close_end['value'];
		if ($meeting_close) {
			$str.= "<br /><br /><br /><span class='user condition warn'>网上会务预约在<span class='user condition big'>".$meeting_close_start."</span>到<span class='user condition big'>".$meeting_close_end."</span>期间，暂停预约服务。如需预约会议室，请致电<span class='user condition big'>83201875</span>电话预约</span>";
		}
		
		
		
		
		
		
		$str.= "</div>";
				$str.= "<div class='meeting_box'><table class='meeting'>";
		$str.= "<tr><td colspan=7 class='meeting_head'>".$year.'年'.$month.'月'."<a href=?".$this->lastMonth($year, $month)." class='previous'>"."上个月"."</a><a href=?".$this->nextMonth($year, $month)." class='next'>"."下个月"."</a></td></tr>";
		$str.= "<tr>";
		for($i = 0;$i < 7;$i++) {
			$str.= "<td class='weekday'>".$week[$i]."</td>";
		}
		$str.= "</tr>";

		/*
		 * 载入预约数据
		 */
		$events = $this->_createEventObj($year, $month);
		//dump($events);exit;

		$str.= "<tr>";

		$tmp_prev_day = $prev_days - $start_weekday + 1; //算出日历每页第一个格子对应是几号

		for($j = 0;$j < $start_weekday;$j++) {
			$str.= "<td class='empty day'>".$tmp_prev_day."</td>";
			$tmp_prev_day++;
		}
		while($k <= $days) {
			/*
			 * 载入每天会议室预约数据
			 */
			$rooms = $this->_createEventObj($year, $month, $k);
			if(isset($events[$k])) {
				$order_num = count($events[$k]);
				if($order_num == 6) {
					$order_info = '<a class="example9" href="javascript:void(0);" title="点击查询">预约已满</a>';
					$class = "full";
				}else {
					$order_info = '<a class="example9" href="javascript:void(0);" title="点击查询">'.count($events[$k])."次预约".'</a>';
					$class = "notfull";
				}
			}else {
				//日历添加暂停功能
				/*if (strtotime($meeting_close_start) <= strtotime("today") && strtotime("today") <= strtotime($meeting_close_end)){
					$order_info = '<a  title="点击查询">暂停预约</a>';
					$class = "notfull";
				}
				else{*/
					$order_info = '<a class="example9" href="javascript:void(0);" title="点击查询">无人预约</a>';
					$class = "free";
				//}
			}

			if($k == date('d') && $month == date('m')) {
				$class = "today";
			}

			//如果预约时间在当天之前或者30天之后则禁止预约
			$orderdate = mktime(0, 0, 0, $month, $k, $year);
			$today = strtotime("today");
			$amoun_days = round(($orderdate - $today)/3600/24);
			if($amoun_days < 0 || $amoun_days > 30) {
				$class = "forbidden";
				$order_info = '<a href="javascript:void(0);"></a>';
			}

			$str.= '<td id='.'now_'.$k." class='".$class ." day'>".$k.'<div class='.$class.'>'.$order_info.'</div>';
			$str.= '<div class="more_inf" style="display: none;">';
			$str.= '<p><strong style="color:red;font-size:14px">您要预约的日期是'.$year.'年'.$month.'月'.$k.'日'.'</strong></p><br>';
			$str.= '<p><strong style="color:red;font-size:14px">网上预约时间:周一至周五8:30~17:30</strong></p><br>';

			$str.= '<div>';
			$str.= '<table class="meeting_room">';
			$str.= '<tr>';
			$str.= '<th ></th>';
			$str.= '<th colspan=2>主楼中335</th>';
			$str.= '<th colspan=2>主楼中324</th>';
			$str.= '<th colspan=2>主楼中322</th>';;
			$str.= '</tr><tr>';
			$str.= '<th >上午</th>';

			if(isset($rooms[335][1])) {
				$str.= '<td><a class="full">'.$rooms[335][1]->apart_name.'</a></td>';
				$str.= '<td><a class="full">'.GetOrderTime($rooms[335][1]->order_time).'</as></td>';
			} else {
				$str.= '<td><a class="free">未预约</a></td>';
				$str.= '<td><a class="free"></a></td>';
			}

			if(isset($rooms[324][1])) {
				$str.= '<td><a class="full">'.$rooms[324][1]->apart_name.'</a></td>';
				$str.= '<td><a class="full">'.GetOrderTime($rooms[324][1]->order_time).'</a></td>';
			} else {
				$str.= '<td><a class="free">未预约</a></td>';
				$str.= '<td><a class="free"></a></td>';
			}

			if(isset($rooms[322][1])) {
				$str.= '<td><a class="full">'.$rooms[322][1]->apart_name.'</a></td>';
				$str.= '<td><a class="full">'.GetOrderTime($rooms[322][1]->order_time).'</a></td>';
			} else {
				$str.= '<td><a class="free">未预约</a></td>';
				$str.= '<td><a class="free"></a></td>';
			}

			$str.= '</tr><tr>';
			$str.= '<th >下午</th>';

			if(isset($rooms[335][2])) {
				$str.= '<td><a class="full">'.$rooms[335][2]->apart_name.'</a></td>';
				$str.= '<td><a class="full">'.GetOrderTime($rooms[335][2]->order_time).'</a></td>';
			} else {
				$str.= '<td><a class="free">未预约</a></td>';
				$str.= '<td><a class="free"></a></td>';
			}

			if(isset($rooms[324][2])) {
				$str.= '<td><a class="full">'.$rooms[324][2]->apart_name.'</a></td>';
				$str.= '<td><a class="full">'.GetOrderTime($rooms[324][2]->order_time).'</a></td>';
			} else {
				$str.= '<td><a class="free">未预约</a></td>';
				$str.= '<td><a class="free"></a></td>';
			}

			if(isset($rooms[322][2])) {
				$str.= '<td><a class="full">'.$rooms[322][2]->apart_name.'</a></td>';
				$str.= '<td><a class="full">'.GetOrderTime($rooms[322][2]->order_time).'</a></td>';
			} else {
				$str.= '<td><a class="free">未预约</a></td>';
				$str.= '<td><a class="free"></a></td>';
			}

			$str.= '</tr></table>';
			$str.= '</div>';

			$str.= '<input type="hidden" class="order_starts"  value='.$year.'-'.$month.'-'.$k.' />';

			$str.= '</p></div>';
			$str.= '</td>';

			if(($j+1) % 7 == 0) {
				$str.= "</tr><tr>";
			}
			$j++;
			$k++;
		}
		$kk = 1;
		while($j % 7 != 0) {
			$str.= "<td class='empty day'>".$kk."</td>";
			$kk++;
			$j++;
		}
		$str.= "</tr>";
		$str.= "</table>";

		return $str;
	}

	function lastMonth($year, $month) {
		if($month == 1) {
			$year = $year - 1;
			$month = 12;
		}else {
			$month--;
		}
		return "year=$year&month=$month";
	}

	function nextMonth($year, $month) {
		if($month == 12) {
			$year = $year + 1;
			$month = 1;
		}else {
			$month++;
		}
		return "year=$year&month=$month";
	}
}
?>