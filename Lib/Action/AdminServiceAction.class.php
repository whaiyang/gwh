<?php
/**
 * @author myj
 *
 */
require_once MYROOT . '/my/conf/db/ServiceConf.php';
$_REQUEST_URI = explode('/',$_SERVER["REQUEST_URI"]);
$APPNAME = $_REQUEST_URI[1];
	
define('WHY_APPNAME',$APPNAME);
define('WHY_DOMAIN',$_SERVER['HTTP_HOST']);
define('PIC_PATH','./Tpl/Public/uploads/files/');
define('PIC_SRC','http://'.$_SERVER['HTTP_HOST'].'/'.WHY_APPNAME.'/Tpl/Public/uploads/files/');

class AdminServiceAction extends AdminAction {

		public function _initialize() {
	        parent::_initialize();
	    }

		function service_list() {
			$type=intval($_GET['type']);
			$map=array();
			if(empty($type)){
				$map['cd_service.type']=array('neq',0);
			}
			else{
				$map['cd_service.type']=array('eq',$type);
			}
			import("ORG.Util.Page");
			$num = D("Service")->where($map)->count();
			$page=new Page($num,15);
			$result['page']=$page->show();
			$result['result']=D("Service")->where($map)->field("cd_service.*, type1.id as type1_id, type1.name as type1_name, type2.id as type2_id, type2.name as type2_name")->join("LEFT JOIN `cd_type` type1  ON type1.id = `cd_service`.`type`")->join("LEFT JOIN `cd_type` type2  ON type2.id = `cd_service`.`stype`")->limit($page->firstRow.','.$page->listRows)->order('cd_service.time DESC')->select();
			$allType = ServiceModel::getAllParentType();
			$this->assign('allType', $allType);
			$this->assign('result',$result['result']);
			$this->assign('page',$result['page']);
			$this->display();
		}
		function add_service() {
			$id = intval($_GET['id']);
			$type = intval($_GET['type']);
			$serviceType = $this->confSheet->where("`sheet` = ".BaseConf::TYPE." AND `field` = '".ServiceConf::$FIELD_NAME['type']."'")->find();
			$result = D("Service")->where("id='$id'")->find();

			$this->assign("parent_id", $serviceType['value']);
			$this->assign("type", $type);
			$this->assign("result", $result);
			$this->display();
		}
		function service_post(){
			$data = array();
			$id = intval($_POST['id']);
			$data['title'] = $_POST['title'];
			$data['type'] = $_POST['type_1'];
			$data['stype'] = $_POST['type_2'];

			$itemArr = array();
			$urlArr= array();
			for ($i=0; $i < (count($_POST) - 3) / 2; $i++) {
				if ($_POST['item_'.$i]) {
				 	array_push($itemArr, $_POST['item_'.$i]);
					array_push($urlArr, urlFormat($_POST['url_'.$i]));
				 } 
			}
			$data['item'] = implode("|", $itemArr);
			$data['link'] = implode("|", $urlArr);
			$data['time'] = time();

			if ($id > 0) {
				$result = D('Service')->where("`id`='".$id."'")->save($data);
			} else {
				$result = D('Service')->add($data);
			}

			if($result) {
				$this->redirect->redirected("添加成功", __APP__.'/AdminService/add_service',1,"#EEE");
			}else {
				$this->redirect->redirected("添加失败", __APP__.'/AdminService/add_service',1,"#EEE");
			}
		}

		/**
			author:WHY
			2014/4/9
		*/

		function category_list()
		{
			/**
			 * @brief 办事大厅二级分类添加
			 */
			$result = ServiceModel::getAllFormType();
			$this->assign('result', $result);
			$this->display();

		}
}
?>