<?php

class MessageAction extends BaseAction{

    function _initialize()
    {
    	parent::_initialize();
       
    }
    function index() {

        import("ORG.Util.Page");    //count总数；num每页显示数

        $num = D("Msg")->where("`read`=1")->count();
        $page=new Page($num,15);
        $result['page']=$page->show();
        $result['result']=D("Msg")->where("`read`=1")->limit($page->firstRow.','.$page->listRows)->order('id DESC')->select();
        $this->assign('title',"用户留言");
        $this->assign("result",$result);

        $this->assign('title',"用户留言");
        $this->display();

    }
    public function verify() {
        import("ORG.Util.Image");
        Image::buildImageVerify();
    }
    function post() {
    	$data = array();

    	$data['username'] = trim(htmlspecialchars($_POST['username']));
    	$data['email'] = trim(htmlspecialchars($_POST['email']));
    	$data['content'] = htmlspecialchars($_POST['content']);
    	$data['time'] = time();
        if($data['content']=="" || $data['username']==""){
            $this->redirect->redirected("请填写内容！",__URL__);
        }

        if (session('verify') != md5($_POST['verify'])) {
            $this->redirect->redirected("验证码错误！", __URL__);
        }

       // dump($data);exit;
 	    $result = D("Msg")->add($data);
        if($result) {
        	$this->redirect->redirected("留言成功,等待管理员审核", __APP__."/Message", 2);
        }else {
        	$this->redirect->redirected("留言失败", __APP__."/Message", 2);
        }
    }
}
?>