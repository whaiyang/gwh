<?php
class EventAction extends Action
{
	/**
	 * 预约ID
	 * @var int
	 *
	 */
	public $id;

	/**
	 * 部门ID
	 * @var int
	 *
	 */
	public $apart_id;

	/**
	 * 部门名称
	 * @var string
	 *
	 */
	public $apart_name;

	/**
	 * 联系方式
	 * @var int
	 *
	 */
	public $phone_num;

	/**
	 * 预约日期
	 * @var datetime
	 *
	 */
	public $order_start;

	/**
	 * 预约时段
	 * @var int
	 *
	 */
	public $order_sel;

	/**
	 * 会议时间
	 * @var int
	 *
	 */
	public $order_time;

	/**
	 * 预约房间号
	 * @var int
	 *
	 */
	public $order_room;

	/**
	 *
	 * 预约描述
	 * @var string
	 */
	public $descript;

	/**
	 * 接受一个预约的数据并储存该数据
	 *
	 * @param array $event 保存预约数据的关联数组
	 * @return void
	 */
	public function __construct($event)
	{
		if(is_array($event))
		{
			$this->id = $event['id'];
			$this->apart_id = $event['apart_id'];
			$this->apart_name = $event['apart_name'];
			$this->phone_num = $event['phone_num'];
			$this->order_start = $event['order_start'];
			$this->order_sel = $event['order_sel'];
			$this->order_time = $event['order_time'];
			$this->order_room = $event['order_room'];
			$this->descript = $event['descript'];
		}
		else
		{
			throw new Exception("No event data was supplied.");
		}
	}
}
?>