<?php
class BaseAction extends EmptyAction {
    /**
     * --------------------
     * 设置redirect,user,msg全局变量(所有model),userid 用户id
     * --------------------
    */
    
    
    public $user;
    
    public $msg;
    
    private $userid;
    
    
    /**
     * -------------------
     * -------------------
	
	 /**
	 ----------------------------------------------------------
	  转向函数，初始化所有Model
	 ----------------------------------------------------------
	 **/
    public function _initialize() {

       parent::_initialize();
       $action=strtolower(MODULE_NAME);
       $this->vister();
       $this->getMenu();
       
       $this->assign("action",$action);
    }
    /**
     * 用户id
     */
    protected function user_id(){
        if(isset($_SESSION['login'])){
            return $this->userid;
        }
        return 0;
        
    }
    function adminer(){
        $user=D("Userbase");
        $map["privitee"]=array(array('eq',8),array('eq',1),'or');
        $adminer=$user->field("id,username,nickname")->where($map)->select();
        $this->assign("adminer",$adminer);
        //dump($adminer);
    }
    /**
     * 检测是否登录
     */
    function check_login(){
        if(!$_SESSION['login'] || !$_SESSION['information']){
            $url=__APP__;
            $this->redirect->redirected("请求的页面不存在",$url);
        }
    }
    /**
     * 检测是否被禁言
     */
    function check_forbid(){
        $user=new UserbaseModel();
        $nowtime=time();//此刻时间
        $id=$this->user_id();//用户id
        $forbid_time=$user->where("id='$id'")->getField("forbid_time");
        if($nowtime<$forbid_time){
            $this->redirect->redirected("您已经被禁言");
        }
    }
    /**
     * 是否是管理员
     */
    function check_admin(){
        
        $id=$_SESSION['information']['id'];//用户id
        $privite=D("User")->where("id='$id'")->find();
      //  dump($_SESSION);
        
        if($privite['privite']==2 || $privite['privite']==3){
              
        }
        else{
            $this->redirect->redirected("非法操作！",__APP__);
        }
    }
    function check_privite(){
        if(empty($_SESSION['login']) || ($_SESSION['information']['privite']==0)){
            $url=__APP__;
            $this->redirect->redirected("请求的页面不存在",$url);
        }
    }
    function Isimage($type){
        switch($type){
               case ".jpg":$ok=1;break;
               case ".gif":$ok=1; break;
               case ".jpeg":$ok=1;break;
               case ".JPEG":$ok=1;break;
               case ".JPG":$ok=1;break;
               case ".GIF":$ok=1;break;
               default:$ok=0;     
            }
        if($ok==0){
            return 0;
        }
        return 1;    
    }
    function download_($filePath , $fileName = null , $mimeType = 'audio/x-matroska' ){
        session_write_close();
        if(empty($filePath) || !file_exists($filePath)){
            echo('<script>alert("文件不存在");history.go(-1);</script>');exit;
        }
        if($fileName == null){
            $fileName=$filePath;
        }
        
        date_default_timezone_set('Asia/Chengdu');
        define('IS_DEBUG',false);
        if (IS_DEBUG) {
        //    $range = "bytes=1000-1999\n2000";
        //    $range = "bytes=1000-1999,2000"; 
        //    $range = "bytes=1000-1999,-2000"; 
        //    $range = "bytes=1000-1999,2000-2999"; 
        }
        Vendor('Transfer');
        $range = isset($_SERVER['HTTP_RANGE'])?$_SERVER['HTTP_RANGE']:null;
        $transfer = new Transfer($filePath,$fileName,$mimeType,$range);
        if (IS_DEBUG) {
            $transfer->setIsLog(true);
        }
        $transfer->send();
    }
    function vister(){
        $recentView=$_COOKIE['viewed'];
        if($recentView==null){
            $recentView=array();
            setcookie("viewed","gwh",time()+60*60,'/');
            D("Vister")->where("`id`=1")->setInc('vister');
        }
        $vister=D("Vister")->where("`id`=1")->find();
        $this->assign('vister',$vister['vister']);
    }
    function getMenu() {
        $menu = M('indexmenu');
		$pmenudata = $menu->where('parent_id = 0')->order("zindex desc")->select();
		$smenudata = $menu->where('parent_id <> 0')->order("zindex desc")->select();
		$menuData['parent'] = $pmenudata;
		$menuData['son'] = $smenudata;
        $this->assign('menu',$menuData); 
    }
   /**
     * @brief分页
     * @param object $model
     * @param array $map 查询条件
     * @param $order 排序
     * @return array page样式 ，数据
     */
    public function showPage($model, $pageSize = 10, $map = null, $order = 'id desc', $join = null, $field = null) {
        if ($model == "") {
            return false;
        }
        import("ORG.Util.Page");    //count总数；num每页显示数
        $num  = $model->where($map)->count();
        $page = new Page($num, $pageSize);
        $result[]   = $page->show();
        $result[] = $model->where($map)->field($field)->join($join)->limit($page->firstRow.','.$page->listRows)->order($order)->select();
        return $result;
        
    }
    /**
     * @author zzq
     * @brief 得到分类
     * @return array <String,Array> js,array
     */
    public function getAllType() {
        $types = D("Type")->order("parent_id ASC")->where("del = 0")->select();
           
        $type1 = array();
        $content_1 = "var type_1=new Array(";
        $content_2 = "var type_2=new Array(";
        
        foreach($types as $key => $value) {
            if($value['parent_id'] == 0) {
                $type1[]   = $value['id'];
                $content_1.= "new Array(".$value['id'].",'".$value['name']."'),";
            } else if(in_array($value['parent_id'], $type1)) {
                $content_2.= "new Array(".$value['parent_id'].",".$value['id'].",'".$value['name']."'),";
            }
        }
        $content = rtrim($content_1, ",").");".rtrim($content_2, ",").");";
        return array($content,$types);
    }
}
?>