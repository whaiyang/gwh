<?php
// 本文档自动生成，仅供测试运行
require_once MYROOT . '/Lib/Action/idstar/ids4Service.php';
class LoginAction extends BaseAction
{
    function _initialize()
	{
        parent::_initialize();
    }
	function index(){
        $this->display();
    }
    function checkAdmin() {
    	$username = $_POST['username'];
    	$password = $_POST['password'];   	
    	$result = D('Login')->where("username='$username'")->find();
    	//dump($result);exit;
    	
    	if(!$result) {
    		$this->redirect->redirected("用户名或密码错误", __APP__, 2);
    	}else {
    		if($result['password'] == $password) {
    			session('admin', $username);
    			session('pass', $password);
    			$this->redirect->redirected("欢迎管理员", __APP__."/Admin", 2);
    		}else {
    			$this->redirect->redirected("用户名或密码错误", __APP__, 2);
    		}
    	}
    }
    function alter() {
    	if(session('admin') == false) {
    		$this->redirect->redirected("非法操作", __APP__, 2);
    	}else {
    		$this->display();
    	}
    }

    //修改管理员账户
    function alter_admin() {
    	$admin = session('admin');
    	$username = $_POST['username'];
    	$password = $_POST['password'];
    	$repassword = $_POST['repassword'];
    	$map = array('username'=>$username, 'password'=>$password);
    	
    	if($password !== $repassword) {
    		$this->redirect->redirected("两次密码不一致,修改失败", __APP__, 2);
    	}
    	
    	$result = D('Login')->where("username='$admin'")->setField($map);
    	
    	if(!$result) {
    		$this->redirect->redirected("修改账户失败", __APP__/Login/alter, 2);
    	}else {
    		$this->redirect->redirected("修改账户成功", __APP__, 2);
    	}
    }

    //修改预约账户
    function alter_order_account() {
        $apart_user = trim(htmlspecialchars($_POST['username']));
        $apart_passwd = trim(htmlspecialchars($_POST['password']));
        $new_passwd = trim(htmlspecialchars($_POST['new_password']));
        $map = array('apart_user'=>$apart_user, 'apart_passwd'=>$apart_passwd);

        $result = D('Apart')->where($map)->setField('apart_passwd',$new_passwd);

        if(!$result) {
            $this->redirect->redirected("修改密码失败", __APP__, 2);
        }else {
            $this->redirect->redirected("修改密码成功", __APP__, 2);
        }
    }

//预约登陆
    function checkOrder() {
        //信息门户api
        $wsdl = "http://222.197.164.13:8080/ids4Api/services/ids4?wsdl";
        $client = new Ids4Service($wsdl,'0Otbty20P94=','focQXymyWFduaN/7GXDEdw==' );

		$username = trim(htmlspecialchars($_POST['username']));
		$password = trim(htmlspecialchars($_POST['password']));
        $apartname = trim(htmlspecialchars($_POST['apartname']));

        $usergroup = $client->getUserGroup($username);
        //如果用户不是教职工，则不能预约
        if ($usergroup->name === "本科生组" || $usergroup->name === "研究生组") {
            $this->redirect->redirected("对不起，会议室预约只对教职工开放", __URL__, 8);
        }

        $result = $client->checkPassword($username, $password);

		if($result) {
			$_SESSION['uid'] = $username;
			$_SESSION['user_name'] = $client->getUserNameByID($username);
            $_SESSION['apart_name'] = $apartname;
			$_SESSION['order_login'] = true;

			$this->redirect->redirected("登陆成功", __APP__."/Index/meeting");
		}else {
			$this->redirect->redirected("登陆失败", __URL__, 5);
		}
	}
    /*function register(){
        if($_SESSION['login']==true){
            $this->redirect->redirected("请您先退出登录,再进行注册！",__APP__,2);
        }
        $this->assign("title","neuro uestc - Register");
        $this->display();
    }
    function post(){
        if($_SESSION['login']==true){
            $this->redirect->redirected("请您先退出登录,再进行注册！",__APP__,2);
        }
        //dump($_POST);exit;
        $data=array();
        $data['username']=trim(htmlspecialchars($_POST['username']));
        $data['password']=htmlspecialchars($_POST['password']);
        $data['repassword']=htmlspecialchars($_POST['repassword']);
        $data['email']=htmlspecialchars($_POST['email']);
        $data['phone']=htmlspecialchars($_POST['phone']);
        $data['English_name']=trim(htmlspecialchars($_POST['English_name']));
        $data['Chinese_name']=trim(htmlspecialchars($_POST['Chinese_name']));
        $data['homepage']=urlFormat(trim(htmlspecialchars($_POST['homepage'])));
        $data['register_time']=time();
        $data['lastlogin_ip']=getIp();
        $data['lastlogin_time']=time();
        $data['leadership']=intval($_POST['leadership']);
        // 如果创建失败 表示验证没有通过 输出错误提示信息
        $result=D("User")->verify($data);
        if($result==-1){

            $data['password']=md5($data['password']);
            unset($data['repassword']);//如果model中关联的话 就必须把不必要的变量清空，否则无法保存，可能是think的bug吧
            $result=D("User")->add($data);
            if($result){
                $_SESSION['admin']=false;
                $_SESSION['login']=true;
                $_SESSION['information']=$data;
                $_SESSION['information']['id']=$result;

                $data=array();
                $data['fromid']=$result;
                $data['content']="用户".$_SESSION['information']['username']."(".$_SESSION['information']['English_name'].")注册为实验室成员,等待您的审核中";
                $data['cduid']=16;
                $data['date']=time();
                D("Msg")->add($data);
                $this->redirect->redirected("注册成功,请先完善资料,等待管理员审核",__APP__."/user/setting",2);
            }
        }
        else{
            $this->redirect->redirected($result,__APP__."/Login/register",4);
        }
    }
    function login(){
        if($_SESSION['login']==true){
            $this->redirect->redirected("您已经登录！",__APP__,2);
        }
        $error=null;
        $username=trim(htmlspecialchars($_POST['username']));
        $password=trim(htmlspecialchars($_POST['password']));
        if($username==""){
            $error.="用户名不能为空,";
        }
        if($password==""){
            $error.="密码不能为空.";
        }
        if($error!=null){
            $this->redirect->redirected($error,__APP__."/Login",2);
        }
        if($error==null){
            $result=D("User")->where("username='$username'")->find();
            if($result){
                if($result['password']==md5($password)){
                    $data=array();
                    $data['lastlogin_ip']=getIp();
                    $data['lastlogin_time']=time();
                    $status=D("User")->where("id='$result[id]'")->save($data);
                    if(!$status){
                        //session_destroy();
                        $this->redirect->redirected("ERROR!",__APP__."/Login",2);
                    }
                    else{
                        $_SESSION['admin']=false;
                        $_SESSION['login']=true;
                        $_SESSION['information']=$result;
                        unset($_SESSION['information']['introduction_study']);
                        unset($_SESSION['information']['introduction_work']);
                        unset($_SESSION['information']['teach_class']);
                        unset($_SESSION['information']['research_main']);
                        if($result['privite']==0){
                            $this->redirect->redirected("登录成功,审核未通过,请与管理员联系！",__APP__,3);
                        }
                        else if($result['privite']==1){
                            $this->redirect->redirected("登录成功！",__APP__,2);
                        }
                        else if($result['privite']==2 || $result['privite']==3){
                            $_SESSION['admin']=true;
                            $this->redirect->redirected("登录成功,欢迎您管理员！",__APP__,2);
                        }
                        else{
                            session_destroy();
                            $this->redirect->redirected("Forbid User!",__APP__."/Login",3);
                        }
                    }

                }
                else{
                    $this->redirect->redirected("用户名或密码错误！",__APP__."/Login",2);
                }
            }
            if(empty($result)){
                $this->redirect->redirected("用户名或密码错误！",__APP__."/Login",2);
            }
        }
        exit("Forbid!");
    }*/
    function out(){
        session_destroy();
        $this->redirect->redirected("退出成功",__APP__,2);
    }

    function order_out(){
        session_destroy();
        $this->redirect->redirected("退出成功",__APP__."/Index/meeting",2);
    }
}
?>
