<?php

class ServiceAction extends BaseAction{

    function _initialize()
	{
        parent::_initialize();

    }
    function index()
    {
        $type = intval($_GET['type']);
        //得到同级别的分类  和当前类别
        $secondType = false;//是否是二级分类
        $typeList = array();//当前类别
        $nowType  = null;//同级别
        if ($type > 0) {
            $t = TypeModel::getTypeById($type);
            if ($t['parent_id'] > 0) { //如果是子类
                $secondType = true;
                $typeList[] = TypeModel::getTypeById($t['parent_id']);
                $nowType    = TypeModel::getChildType($t['parent_id']);
            } else {
                $nowType = TypeModel::getChildType($t['id']);
            }
            $typeList[] = $t;
        }
       
        $this->assign('typeList', $typeList);
        $this->assign('nowType', $nowType);

        $result = TypeModel::getChildType($type);
        foreach ($result as $key => $value) {
            $result[$key]['data'] = D("Service")->where("`stype` = '".$value['id']."'")->select();
        }
        $this->assign('result', $result);
        $this->display();
    }
}
?>