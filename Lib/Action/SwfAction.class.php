<?php
class SwfAction extends ImageAction{
    function index(){
        
        set_time_limit(0);
       	if (isset($_POST["PHPSESSID"])) {
    		session_id($_POST["PHPSESSID"]);
    	} else if (isset($_GET["PHPSESSID"])) {
    		session_id($_GET["PHPSESSID"]);
    	}
   
    	session_start();
    // Check post_max_size (http://us3.php.net/manual/en/features.file-upload.php#73762)
        if(!isset($_SESSION['admin']) || !$_SESSION['admin']){
			echo 3;exit(0);
		}
    	$POST_MAX_SIZE = ini_get('post_max_size');
    	$unit = strtoupper(substr($POST_MAX_SIZE, -1));
    	$multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));
    
    	if ((int)$_SERVER['CONTENT_LENGTH'] > $multiplier*(int)$POST_MAX_SIZE && $POST_MAX_SIZE) {
    		header("HTTP/1.1 500 Internal Server Error"); // This will trigger an uploadError event in SWFUpload
    		echo 1;//"POST exceeded maximum allowed size.";
    		exit(0);
    	}
    
    // Settings
        $time=date("Y-m");
    	$save_path = dirname(__FILENAME__) . "/uploads/".$time;
        
        if(!is_dir($save_path))
        { 
            mkdir($save_path);
        }	
    	// The path were we will save the file (getcwd() may not be reliable and should be tested in your environment)
    	$upload_name = "Filedata";
    	$max_file_size_in_bytes = 83886080;				// 2GB in bytes
    	$extension_whitelist = array("jpeg", "jpg", "gif", "bmp");	// Allowed file extensions
    	
    // Other variables	
    	$MAX_FILENAME_LENGTH = 260;
    	$file_name = "";
    	$file_extension = "";
    	/*
        $uploadErrors = array(
            0=>"There is no error, the file uploaded successfully",
            1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
            2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
            3=>"The uploaded file was only partially uploaded",
            4=>"No file was uploaded",
            6=>"Missing a temporary folder"
    	);
    */
    
    // Validate the upload
    	if (!isset($_FILES[$upload_name])) {
    		echo 2;//$this->HandleError("No upload found in \$_FILES for " . $upload_name);
    		exit(0);
    	} else if (isset($_FILES[$upload_name]["error"]) && $_FILES[$upload_name]["error"] != 0) {
    		echo 3;//$this->HandleError($uploadErrors[$_FILES[$upload_name]["error"]]);
    		exit(0);
    	} else if (!isset($_FILES[$upload_name]["tmp_name"]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"])) {
    		echo 4;//$this->HandleError("Upload failed is_uploaded_file test.");
    		exit(0);
    	} else if (!isset($_FILES[$upload_name]['name'])) {
    		echo 5;//$this->HandleError("File has no name.");
    		exit(0);
    	}
    	
    // Validate the file size (Warning: the largest files supported by this code is 2GB)
    	$file_size = @filesize($_FILES[$upload_name]["tmp_name"]);
    	if (!$file_size || $file_size > $max_file_size_in_bytes) {
    		echo 1;
    		exit(0);
    	}
    
    	if ($file_size <= 0) {
    		echo 0;//$this->HandleError("File size outside allowed lower bound");
    		exit(0);
    	}
  
    	$path_info = pathinfo($_FILES[$upload_name]['name']);
    	$file_extension = $path_info["extension"];
    	$is_valid_extension = false;
    	foreach ($extension_whitelist as $extension) {
    		if (strcasecmp($file_extension, $extension) == 0) {
    		    $type_=$extension;
    			$is_valid_extension = true;
    			break;
    		}
    	}
    	if (!$is_valid_extension) {
    		echo 6;//$this->HandleError("Invalid file extension");
    		exit(0);
    	}
        $file_name=time().mt_rand(100000,999999).".".$type_;
    
    	 
    	if (!@move_uploaded_file($_FILES[$upload_name]["tmp_name"], $save_path."/".$file_name)) {
    		echo 71;//$this->HandleError("File could not be saved.");
    		exit(0);
    	}
    	else{
          $this->save('uploads',$file_name,$time);//�����΢ͼ
            
         
          echo $save_path."/".$file_name;exit;
    	}
    	exit(0);
   }
   function HandleError($message) {
	   echo $message;
   }
}