<?php
/**
 * @author zzq
 *
 */
require_once MYROOT . '/my/conf/db/ImageConf.php';

class AdminImageAction extends AdminAction {

    public function _initialize() {
        parent::_initialize();
    }
	
    public function image() {
        
        $id        = intval($_GET['id']);
        $imageType = $this->confSheet->where("`sheet` = ".BaseConf::IMAGE." AND `field` = '".ImageConf::$FIELD_NAME['type']."'")->select();
        $result    = D("Image")->where("id='$id'")->find();
        
        $this->assign('imageType', $imageType);
        $this->assign('result', $result);
        $this->display();
    }
    /**
     * 
     */
    public function imageList() {
        
        $imageType = $this->confSheet->where("`sheet` = ".BaseConf::IMAGE." AND `field` = '".ImageConf::$FIELD_NAME['type']."'")->select();
        $type      = intval($_GET['type']);
        $image     = D("Image");
        if ($type == 0) {
            $map['cd_image.type'] = array('neq', 0);
        } else {
            $map['cd_image.type'] = array('eq', $type);
        }
        import("ORG.Util.Page");
        $num  = $image->where($map)->count();
        $page = new Page($num, 15);
        $result['page']   = $page->show();
        $result['result'] = $image->where($map)->field("cd_image.*, cd_conf.value, cd_conf.name")->join("cd_conf ON cd_image.type = cd_conf.value AND cd_conf.sheet = ".BaseConf::IMAGE)->limit($page->firstRow.','.$page->listRows)->order('cd_image.id DESC')->select();
        $this->assign('imageType', $imageType);
        $this->assign('result',    $result['result']);
        $this->assign('page',      $result['page']);
        $this->display();
    }
    /**
     * 
     */
    public function imagePost() {
        $id   = intval($_POST['id']);
        $flag = 0;
        $type = intval($_POST['type']);
        if(count($_POST['imagess']) < 1) {
            HttpNamespace::redirected("ERROR2！",__URL__."/image",2,"#EEE");
        }

        for($i=0; $i < count($_POST['imagess']); $i++) {
            $flag = 1;
            $data = array();
            $data['title']=htmlspecialchars($_POST['title_'.$i]);
            if($_POST['link_'.$i] == null || $_POST['link_'.$i] == " ") {
                $_POST['link_'.$i] = " ";
            } else {
                $data['link'] = urlFormat(htmlspecialchars($_POST['link_'.$i]));
            }

            $data['image'] = htmlspecialchars($_POST['imagess'][$i]);
            $filename      = basename($data['image']);
            $filepath      = dirname($data['image']);
            if(is_file($filepath.'/thumb-'.$filename)) {
                $data['thumb'] = $filepath.'/thumb-'.$filename;
            } else {
                $data['thumb'] = $data['image'];
            }
            $data['user_id'] = $_SESSION['user']['id'];
            $data['time']    = time();
            $data['type']    = $type;
            //是否是更新
            if ($id > 0) {
                $result = D("Image")->data($data)->where("`id` = " .$id)->save();
            } else {
                $result = D("Image")->add($data);
            }
            //
            if(!$result) {
                HttpNamespace::redirected("保存失败",__URL__."/image",2,"#EEE");
            }
        }


        if($flag==0){
            HttpNamespace::redirected("您没有做修改",__URL__."/image",2,"#EEE");
        }
        HttpNamespace::redirected("修改成功",__URL__."/image",2,"#EEE");
    }
    /**
     *
     */
    public function imageType() {
        $imageType = $this->confSheet->where("`sheet` = ".BaseConf::IMAGE." AND `field` = '".ImageConf::$FIELD_NAME['type']."'")->select();
      
        $this->assign('imageType', $imageType);
        $this->display();
    }
}