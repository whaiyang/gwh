<?php
/**
 * @author myj
 *
 */


class AdminArticleAction extends AdminAction {
        
        public $publication;//文章
        public function _initialize() {
            parent::_initialize();
            $this->publication = D("Publication");
            
        }

        public function article(){
            $type=intval($_GET['type']);
            $map=array();
            if(empty($type)){
                $map['cd_publication.type']=array('neq',0);
            } else {
                $map['cd_publication.type']=array('eq',$type);
            }
            import("ORG.Util.Page");
            $num = $this->publication->where($map)->count();
            $page=new Page($num,15);
            $result['page']=$page->show();
            $result['result']=$this->publication->where($map)->field("cd_publication.*, type1.id as type1_id, type1.name as type1_name, type2.id as type2_id, type2.name as type2_name")->join("LEFT JOIN `cd_type` type1  ON type1.id = `cd_publication`.`type`")->join("LEFT JOIN `cd_type` type2  ON type2.id = `cd_publication`.`type_2`")->limit($page->firstRow.','.$page->listRows)->order('cd_publication.time DESC')->select();
            //得到 所有文章类型
            $allType = TypeModel::getAllParentType();
            $this->assign('allType', $allType);
            
            $this->assign('result',$result['result']);
            $this->assign('page',$result['page']);
            $this->display();
        }

        public function add_article(){
            $id     = intval($_GET['id']);
            $type   = intval($_GET['type']);//判断添加的是Publication（1）还是 新闻（2）
            $result = $this->publication->where("id='$id'")->find();
            //zzq 文章类型添加
           
            list($content, $types) = $this->getAllType();

            $this->assign("content",$content);
            $this->assign("type",$type);
            $this->assign("types",$types);
            $this->assign("result",$result);
            $this->display();
        }

        public function ue_post() {
//dump($_POST);exit();
            $data           = array();
            $id             = intval($_POST['id']);
            $data['title']  = $_POST['title'];
            $data['type']   = intval($_POST['type_1']);
            $data['type_2'] = intval($_POST['type_2']);
            $data['author'] = $_POST['author'];
            $data['source'] = $_POST['source'];
            if ($_POST['color']) {
                $data['color']  = strtotime($_POST['color']);
            }
            if(strpos($_POST['myContent'],"<img")) {
                $data['pic'] = 1;
            }
            $data['private'] = 1;
            $data['content'] = htmlspecialchars($_POST['myContent']);
            
            if($id > 0) {
                $result = $this->publication->where("`id`= '".$id."' AND `type` != 0")->save($data);
            }else{
                $data['time']    = time();
                $result = $this->publication->add($data);
            }

            if($result) {
                $this->redirect->redirected("添加成功", __URL__."/add_article",1);
            }else {
                $this->redirect->redirected("添加失败", __URL__."/add_article",1);
            }
        }
        /**
         * @author zzq
         * @brief 文章类型添加
         */
        public function addType() {
            $result = TypeModel::getAllFormType();
            $this->assign('result', $result);
            $this->display();
        }
       
        public function editType()
        {
            var_dump($_GET);
        }

        public function putLeft()
        {
            $aid = $_GET['aid'];
            if(empty($aid))
                $this->error("数据缺失(aid)");
            $extra = $this->publication->where(array('id'=>$aid))->limit(1)->select();
            if(empty($extra))
                $this->error("修改失败(select)");
            $extra = $extra[0]['extra'];
            if($extra=='left')
                $extra = 'right';
            else if($extra=='right'||empty($extra))
                $extra = 'left';
            if($this->publication->where(array('id'=>$aid))->save(array('extra'=>$extra)))
                $this->success("修改成功");
            else
                $this->error("修改失败(save)");
        }
}
?>