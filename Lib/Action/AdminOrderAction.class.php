<?php
/**
 * @author myj
 *
 */
require_once MYROOT . '/my/conf/db/MeetingConf.php';

class AdminOrderAction extends AdminAction {

	public function _initialize() {
        parent::_initialize();
    }

	function order() {
		$start_date = date("Y-m-d");
		$end_date = date("Y-m-d", time() + 30*24*3600);
		$map['order_start'] = array('between', array($start_date, $end_date));

		$Total_num = D("Meeting")->where($map)->count();
		$add_num = D("Meeting")->where("type=1")->count();
		$actual_num = D("Meeting")->where("type=0 AND authority=1")->count();
		$result = D("Meeting")->showpage($Total_num, 20, $start_date, $end_date);

		$this->assign('result',$result[0]);
		$this->assign('page',$result[1]);
		$this->assign('Total_num',$Total_num);
		$this->assign('add_num',$add_num);
		$this->assign('actual_num',$actual_num);
		$this->display();
	}

	function add_order() {
		$webdata = D("Webdata");
		$meeting_close = $webdata->where(array('key'=>'meeting_close'))->find();
		$meeting_close_start = $webdata->where(array('key'=>'meeting_close_start'))->find();
		$meeting_close_end = $webdata->where(array('key'=>'meeting_close_end'))->find();
		if($meeting_close['value']==1)
			$meeting_close = "checked='checked'";
		else
			$meeting_close = "";

		$this->assign("meeting_close",$meeting_close);
		$this->assign("meeting_close_start",$meeting_close_start);
		$this->assign("meeting_close_end",$meeting_close_end);
		$this->display();
	}

	function order_close()
	{
		$webdata = D("Webdata");
		if(empty($_POST['meeting_close']))
			$_POST['meeting_close'] = "0";
		$f1=$webdata->where(array('key'=>'meeting_close'))->save(array('value'=>$_POST['meeting_close']));
		$f2=$webdata->where(array('key'=>'meeting_close_start'))->save(array('value'=>$_POST['meeting_close_start']));
		$f3=$webdata->where(array('key'=>'meeting_close_end'))->save(array('value'=>$_POST['meeting_close_end']));
		if($f1||$f2||$f3)
		{
			$this->success("编辑成功");
		}
		else
		{
			$this->error("编辑失败</br>".$webdata->getError());
		}
	}

	function add_order_post() {
		$data = array();
		$data['apart_id'] = intval($_POST['apart_id']);
		$data['apart_name'] = trim($_POST['apart_name']);
		$data['contacts'] = trim($_POST['contacts']);
		$data['phone_num'] = intval($_POST['phone_num']);
		$data['order_start'] = trim($_POST['order_start']);
		$data['order_room'] = trim($_POST['order_room']);
		$data['video_sel'] = intval($_POST['video_sel']);
		if(intval($_POST['order_time']) > 0 && intval($_POST['order_time']) < 4) {
				$data['order_sel'] = 1;
			}
			if(intval($_POST['order_time']) > 3) {
				$data['order_sel'] = 2;
			}
		$data['order_time'] = trim($_POST['order_time']);
		$data['descript'] = trim($_POST['descript']);
		$data['type'] = 1;
		$data['authority'] = 1;

		$result = D("Meeting")->add($data);

		if($result) {
			$this->redirect->redirected("添加成功", __URL__."/add_order", 2);
		} else {
			$this->redirect->redirected("添加失败", __URL__."/add_order", 2);
		}
	}
}
?>