<?php
class ShowCommentWidget extends Widget{     

    public function render($data){

       echo <<<EOT
           <link media="screen" rel="stylesheet" href="../Public/css/colorbox.css" /> 
    	   <script src="../Public/js/jquery.colorbox.js"></script>
    	  <script>
		$(document).ready(function(){
			$(".example8").colorbox({
			  'width':660,
              'height':400,
              'opacity': 0,
              'inline' : true,
              'href':"#inline_example1"
               });
			$(".example9").colorbox({
			  'width':700,
              'height':700,
              'opacity': 0,
              'inline' : true,
              'href':"#inline_example2"
               });
		});
	</script>
    
    <div style='display:none'> 
		<div id='inline_example1' style='padding:10px; background:#fff;'> 
	       <div id='contentHtml'></div>
               <textarea style="width: 400px;height: 150px;" id='replayContent'></textarea>
                <input name="sub" value="提交" class="subed" type="button" />
		</div> 
	</div>

	<div style='display:none'>
		<div id='inline_example2' style='padding:10px; background:#fff;'>
			<div id='contentOrder'></div>
			<br><p style="width:570px;" class="tited"><strong style="font-size: 18px;color: #06C;">预约信息:(请选择当天没有被预约的会议室和会议时段)</strong><br><br>
			<form name="order_form" id="order_form" method="post" action="__APP__/Index/order_post">
				<input type="hidden" id="order_start" name="order_start" value="" />
					<p><strong>预约会议室:</strong>
					<select id="order_room" name="order_room">
						<option value=""></option>
						<option value="335">主楼中335</option>
						<option value="324">主楼中324</option>
						<option value="322">主楼中322</option>
					</select></p><br>
					<div id="vedio_select" style="display:none;">
						<p><strong>视频会议:&nbsp;&nbsp;</strong>
						<select name="video_sel">
							<option value="0">否</option>
							<option value="1">是</option>
						</select></p><br>
					</div>
					<p><strong>预约时段:</strong>&nbsp;&nbsp
					<select id="order_time" name="order_time">
						<option value=""></option>
						<option value="3">8:30~12:00</option>
						<option value="6">14:30~18:00</option>
					</select></p><br>
					<p><strong>参会人数:</strong>&nbsp;&nbsp;
					<select id="descript" name="descript">
						<option value=""></option>
						<option value="1">小于20人</option>
						<option value="2">20到30人</option>
						<option value="3">30到40人</option>
					</select></p><br>
					<p><strong>联系方式:</strong>&nbsp;&nbsp
						<input type="text" name="phone_num" id="phone_num" />
					</p><br>
					<p>&nbsp;&nbsp;
						<input type="submit" value="提交" onclick="return checkForm();" />
						<input type="reset" value="重置" style="display:none;" />
					</p>
			</form>
		</div>
	</div>
    	
EOT;
    }     

}  
?>