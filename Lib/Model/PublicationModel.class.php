<?php
class PublicationModel extends RelationModel{
    protected $_link = array(
        'user'=>array(
        'mapping_type' =>BELONGS_TO,
        'class_name' =>'User',
        'foreign_key'=>'user_id',
        'as_fields'=>'email,English_name'
        ),
);
    function showpage($count,$num,$user_id){
        import("ORG.Util.Page");    //count总数；num每页显示数
        $listRows=$num;
        $Page=new Page($count,$num);
        $show=$Page->show();
        $list=$this->where("user_id='$user_id' AND class=0")->limit($Page->firstRow.','.$Page->listRows)->order("id desc")->select();
        $arr[0]=$list;
        $arr[1]=$show;
         return $arr;
    }
     function showpage_admin($count,$num,$map){
        import("ORG.Util.Page");    //count总数；num每页显示数
        $listRows=$num;
        $Page=new Page($count,$num);
        $show=$Page->show();
        $list=$this->where($map)->limit($Page->firstRow.','.$Page->listRows)->order("id desc")->select();
        $arr[0]=$list;
        $arr[1]=$show;
         return $arr;
    }
}
?>