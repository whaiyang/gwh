<?php

class ImageModel extends RelationModel{

    function showpage_admin($count,$num,$map){
        import("ORG.Util.Page");    //count总数；num每页显示数
        $listRows=$num;
        $Page=new Page($count,$num);
        $show=$Page->show();
        $list=$this->where($map)->limit($Page->firstRow.','.$Page->listRows)->order("id desc")->select();
        $arr[0]=$list;
        $arr[1]=$show;
         return $arr;
    }
}
?>