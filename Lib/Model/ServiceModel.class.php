<?php

class ServiceModel extends Model{

    public static $instance = null;

    public static function init() {
        self::getInstance();
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = M("Type");
        }
        return '';
    }

    /**
     * @brief 得到所有办事大厅二级分类(parent_id=6)
     * Enter description here ...
     */
    public static function getAllParentType() {
        self::init();
        return self::$instance->where("`parent_id` = 6 AND `del` = 0")->order("id ASC")->select();
    }

    /**
     * @brief 得到所有分类 组织好 [child]形式
     * 
     */
    public static function getAllFormType() {
        
        $result = self::getAllParentType();
        foreach($result as $key => $value) {
            $result[$key]['child'] =self::$instance->where("`parent_id` = " . $value['id'] ." AND  `del` = 0")->order("id ASC")->select();
        }
        return $result;
    }

    /**
     * @brief 得到所有分类
     * @return array <String,Array> js,array
     */
    public static function getAllTypeById($show = true, $id = null) {

        self::init();

        if (!$show) {
            $types = self::$instance->order("parent_id ASC")->where("`parent_id` = '".$id."' AND `show` = 0")->select();
        } else {
            $types = self::$instance->order("parent_id ASC")->where("`parent_id` = '".$id."' AND `del` = 0")->select();
        }

        foreach ($types as $key => $value) {
            $childType = self::$instance->where("`parent_id` = '".$value['id']."' AND `del` = 0")->select();
            if ($childType) {
                $types = array_merge($types, $childType);
            }
        }

        return $types;
    }


    function showpage_admin($count,$num,$map){
        import("ORG.Util.Page");    //count总数；num每页显示数
        $listRows=$num;
        $Page=new Page($count,$num);
        $show=$Page->show();
        $list=$this->where($map)->limit($Page->firstRow.','.$Page->listRows)->order("id desc")->select();
        $arr[0]=$list;
        $arr[1]=$show;
         return $arr;
    }
}
?>