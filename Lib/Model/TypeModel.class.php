<?php
/**
 * @author zzq
 * @brief Type 表
 */
class TypeModel extends Model {
    
    public static $instance = null;
    
    public static function init() {
        self::getInstance();
    }
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = M("Type");
        }
        return '';
    }
    /**
     * @brief 得到所有一级分类
     * Enter description here ...
     */
    public static function getAllParentType() {
        self::init();
        return self::$instance->where("`parent_id` = 0 AND `del` = 0")->order("id ASC")->select();
    }
    /**
     * @brief 得到所有分类 
     * @param boolen 是否展示部分类别 true 所有
     */
    public static function getAllType($show = true) {
        self::init();
        $map['del'] = array('eq', 0);
        if (!$show) {
            $map['show'] = array('eq', 0);
        }
        return self::$instance->order("parent_id ASC,id ASC")->where($map)->select();
    }
    /**
     * @brief 得到所有分类 组织好 [child]形式
     * 
     */
    public static function getAllFormType() {
        
        $result = self::getAllParentType();
        foreach($result as $key => $value) {
            $result[$key]['child'] =self::$instance->where("`parent_id` = " . $value['id'] ." AND  `del` = 0")->order("id ASC")->select();
        }
        return $result;
    }
    /**
     * @brief 得到子类
     * @param int $parentType 父类id
     */
    public static function getChildType($parentType = null) {
        self::init();
        if (!$parentType) {
            return null;
        }
        return self::$instance->where("`parent_id` = " . $parentType . " AND `del` = 0")->order("`id` asc")->select();
    
    }
    /**
     * @brief 得到id对应的条
     */
    public static function getTypeById($id = null) {
        if (!$id) {
            return null;
        }
        self::init();
        return self::$instance->where("`id` = " . $id)->find();
    }
    /**
     * @brief 得到文章对应的分类 包括二级分类。有可能是删除的分类
     * @param array $types<一级，二级>分类id
     */
    public static function getArticleTypes($types) {
        self::init();
        return self::$instance->where("`id` = " . $types[0] . " OR `id` = " . $types[1])->order("`parent_id` ASC")->select();
    } 
}